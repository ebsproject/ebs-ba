const path = require('path');
const { merge } = require('webpack-merge');
const singleSpaDefaults = require('webpack-config-single-spa-react');
const DotenvWebpackPlugin = require('dotenv-webpack');

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: 'ebs',
    projectName: 'ba',
    webpackConfigEnv,
    argv,
    // standaloneOptions: {
    //   importMap: {
    //     imports: {
    //       '@ebs/styleguide': 'http://localhost:8081/ebs-styleguide.js',
    //       'cs-ui': '//localhost:3000/static/js/bundle.js',
    //     },
    //   },
    // },
  });

  // add to defaultConfig externals 'cs-ui'
  defaultConfig.externals.push(
    'cs-ui',
    '@ebs/components',
    /@material-ui\/core.*/,
    /@material-ui\/icons.*/,
    /@material-ui\/styles.*/,
  );

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
    resolve: {
      alias: {
        assets: path.resolve(__dirname, './src/assets/'),
        components: path.resolve(__dirname, './src/components/'),
        mock: path.resolve(__dirname, './src/mock/'),
        pages: path.resolve(__dirname, './src/pages/'),
        reducks: path.resolve(__dirname, './src/reducks/'),
        utils: path.resolve(__dirname, './src/utils/'),
      },
    },
    plugins: [new DotenvWebpackPlugin()],
  });
};
