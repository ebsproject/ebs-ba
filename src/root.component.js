import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import { EBSMaterialUIProvider } from '@ebs/styleguide';
import { InstanceContext, InstanceProvider } from 'cs-ui';
// container
import MainRequestView from 'pages/MainRequest/mainrequestview';
import RequestAdd from 'pages/RequestAdd';
// mock data for development and test purposes
import { configureStore } from 'reducks';
const store = configureStore();
// mock data for development and test purposes
import './mock';
import { ROUTE_ADD_REQUEST, ROUTE_LIST_REQUEST } from 'utils/configDefault';

export default function App() {

  return (
    <EBSMaterialUIProvider>
      <Provider store={store}>
        <BrowserRouter>
          <Route
            path='/ba'
            render={({ match }) => (
              <>
                <Route
                  exact
                  path={match.url + ROUTE_ADD_REQUEST}
                  component={RequestAdd}
                />
                <Route
                  exact
                  path={match.url + ROUTE_LIST_REQUEST}
                  component={MainRequestView}
                />
              </>
            )}
          ></Route>
        </BrowserRouter>
      </Provider>
    </EBSMaterialUIProvider>
  );
}
