import mock from '../utils/mock'

mock.onGet('/profile').reply(200, {
  get_profile: {
    data: {
      count: 1,
      user_data: {
        data: [
          {
            id: 1,
            firts_name: 'Diana',
            last_name: 'Herrera',
            email: 'alhe@gmail.com',
            password: 'secret',
          },
        ],
      },
    },
  },
})
