import Axios from 'axios';

const accessToken = localStorage.getItem('id_token');
const instance = Axios.create({
  baseURL: 'https://cbapi-dev.ebsproject.org/v3/',
  headers: {
    Authorization: `Bearer ${accessToken}`,
  },
});

export default instance;
