import { LogglyTracker } from 'loggly-jslogger'

const logger = new LogglyTracker()
logger.push({ logglyKey: 'ARM-KEY-LOG' })
export default logger
