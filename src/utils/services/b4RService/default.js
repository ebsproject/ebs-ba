import clientAxios from 'utils/clientAxios'

export async function getPrediction() {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
      },
    },
  }
  dataSet.data = [
    { id: 1, title: 'G' },
    { id: 2, title: 'GxE' },
  ]
  dataSet.status.status = 200
  return dataSet
}
