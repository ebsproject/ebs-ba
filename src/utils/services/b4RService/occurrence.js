import logger from 'utils/logger'
import clientAxios from 'utils/clientAxios'
import {
  getUrlB4rOccurrencesSearch,
  getUrlB4rOccurrencesPlotDataSearch,
  SORT_OCCURRENCEDBID_DESC,
} from 'utils/helpers/b4rHelper'

export async function getOccurrenceByExperimentDbId(
  experimentDbId,
  limit = 10,
  page = 1,
) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
      },
    },
  }
  const parameter = {
    experimentDbId: `${experimentDbId}`.toString(),
  }
  try {
    let url = getUrlB4rOccurrencesSearch(limit, page, SORT_OCCURRENCEDBID_DESC)
    let result = await clientAxios.post(url, parameter)
    dataSet.data = result.data.result.data
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages
    dataSet.status.status = 200
    // > Inyect dummy data
    //let result = experimentDummy
    //dataSet.data = result
    //dataSet.metadata.pagination.totalPages = 1
    // < End
  } catch (ex) {
    console.log(`Error axios - Occurrence by Experiment:`.toString(), ex)
    logger.push(JSON.stringify({ type: 'error', message: ex }))
    dataSet.status.status = ex.response.status
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view occurrence.'
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No occurrence found.'
    } else if (500 <= ex.response.status) {
      dataSet.status.message = 'Unable to view occurrence.'
    }
  }
  return dataSet
}

export async function getPlotDataByOccurrenceDbId(occurrenceDbId) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
      },
    },
  }
  const parameter = {
    distinctOn: 'variableDbId',
  }
  try {
    let url = getUrlB4rOccurrencesPlotDataSearch(occurrenceDbId)
    let result = await clientAxios.post(url, parameter)
    dataSet.data = result.data.result.data
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages
    dataSet.status.status = 200
  } catch (ex) {
    console.log(`Error axios - Plot data by Occurrence:`.toString(), ex)
    logger.push(JSON.stringify({ type: 'error', message: ex }))
    dataSet.status.status = ex.response.status
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view occurrences.'
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No occurrences found.'
    } else if (500 <= ex.response.status) {
      dataSet.status.message = 'Unable to view occurrences.'
    }
  }
  return dataSet
}

export async function getOccurrenceWithTraitByExperimentDbId(experimentDbId) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
      },
    },
  }
  let result = await getOccurrenceByExperimentDbId(experimentDbId)
  if (result.status.status === 200) {
    for (let element of result.data) {
      if (dataSet.status.status === 100) {
        let trait = await getPlotDataByOccurrenceDbId(element.occurrenceDbId)
        if (trait.status.status === 200) {
          if (trait.data[0].plotData.length > 0) {
            element.traits = trait.data[0].plotData.map((item) => {
              return {
                variableDbId: item.variableDbId,
                variableAbbrev: item.variableAbbrev,
              }
            })
          } else {
            element.traits = []
          }
        } else {
          dataSet.status.status = trait.status.status
          dataSet.status.message = trait.status.message
        }
      }
    }
  } else {
    dataSet.status.status = result.status.status
    dataSet.status.message = result.status.message
  }
  if (dataSet.status.status === 100) {
    dataSet.status.status = 200
    dataSet.data = result.data
  }
  return dataSet
}

export function intersectionTrait(newTrait, cumulativeTraits) {
  //let traits = cumulativeTraits[0]
  let result = { isIntersection: false, traits: cumulativeTraits[0] }
  if (cumulativeTraits.length === 0) {
    result.isIntersection = true
    result.traits = newTrait
  } else {
    for (let cumulativeTrait of cumulativeTraits) {
      result.traits = cumulativeTrait.filter((item) =>
        result.traits.map((trait) => trait.variableDbId).includes(item.variableDbId),
      )
    }
    if (
      newTrait.filter((item) =>
        result.traits.map((trait) => trait.variableDbId).includes(item.variableDbId),
      ).length > 0
    ) {
      result.isIntersection = true
    }
  }
  return result
}

export function getIntersectionTraitFromOccurrences(occurrences) {
  let traits = []
  let traitsOccurrence = occurrences.map((occurrence) => occurrence.traits)

  if (traitsOccurrence.length > 0) {
    traits = traitsOccurrence[0]
    for (let cumulativeTrait of traitsOccurrence) {
      traits = cumulativeTrait.filter((item) =>
        traits.map((trait) => trait.variableDbId).includes(item.variableDbId),
      )
    }
  }
  traits = traits.map((trait) => {
    return { id: trait.variableDbId, title: trait.variableAbbrev }
  })
  return traits
}

export const occurenceColumns = [
  { headerName: 'occurrenceDbId', field: 'occurrenceDbId', hide: true },
  { headerName: 'programDbId', field: 'programDbId', hide: true },
  { headerName: 'projectDbId', field: 'projectDbId', hide: true },
  { headerName: 'siteDbId', field: 'siteDbId', hide: true },
  { headerName: 'stageDbId', field: 'stageDbId', hide: true },
  { headerName: 'experimentDbId', field: 'experimentDbId', hide: true },
  { headerName: 'seasonDbId', field: 'seasonDbId', hide: true },
  { headerName: 'dataProcessDbId', field: 'dataProcessDbId', hide: true },
  {
    headerName: 'locationDbId',
    field: 'locationDbId',
    hide: true,
  },
  {
    headerName: 'Experiment',
    field: 'experiment',
    hide: false,
    sortable: true,
    flex: 1,
  },
  {
    headerName: 'Location',
    field: 'location',
    hide: false,
    sortable: true,
    flex: 1,
  },
  {
    headerName: 'Occurrence',
    field: 'occurrenceName',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
  {
    headerName: 'Plant Area',
    field: 'location',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
  {
    headerName: 'Phase',
    field: 'experimentStage',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
  {
    headerName: 'Year',
    field: 'experimentYear',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
  {
    headerName: 'Design',
    field: 'experimentDesignType',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
  {
    headerName: 'Entry Type (*)',
    field: 'entryType',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
  {
    headerName: 'N. Entries',
    field: 'entryCount',
    hide: false,
    sortable: true,
    flex: 0.6,
  },
  {
    headerName: 'Harvest (*)',
    field: 'harvestStatus',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
]
