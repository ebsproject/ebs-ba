import clientAxios from 'utils/clientAxios'
import { getUrlB4rExperimentSearch } from 'utils/helpers/b4rHelper'
import logger from 'utils/logger'

export async function getExperiment(limit, page) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  }
  const parameter = {
    experimentStatus: 'planted|observation',
    experimentType: 'Breeding Trial',
  }
  try {
    let url = getUrlB4rExperimentSearch(limit, page)
    let result = await clientAxios.post(url, parameter)
    dataSet.data = result.data.result.data
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages
    dataSet.metadata.pagination.totalCount =
      result.data.metadata.pagination.totalCount
    dataSet.status.status = 200
    // > Inyect dummy data
    //let result = experimentDummy
    //dataSet.data = result
    //dataSet.metadata.pagination.totalPages = 1
    // < End
  } catch (ex) {
    console.log(`Error axios - Experiments:`.toString(), ex)
    logger.push(JSON.stringify({ type: 'error', message: ex }))
    dataSet.status.status = ex.response.status
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view experiments.'
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No experiments found.'
    } else if (500 <= ex.response.status) {
      dataSet.status.message = 'Unable to view experiments.'
    }
  }
  return dataSet
}

export const experimentColumns = [
  { headerName: 'programDbId', field: 'programDbId', hide: true },
  { headerName: 'projectDbId', field: 'projectDbId', hide: true },
  { headerName: 'siteDbId', field: 'siteDbId', hide: true },
  { headerName: 'stageDbId', field: 'stageDbId', hide: true },
  { headerName: 'experimentDbId', field: 'experimentDbId', hide: true },
  { headerName: 'seasonDbId', field: 'seasonDbId', hide: true },
  { headerName: 'dataProcessDbId', field: 'dataProcessDbId', hide: true },
  {
    headerName: 'locationDbId',
    field: 'locationDbId',
    hide: true,
  },

  {
    headerName: 'Experiment',
    field: 'experimentName',
    hide: false,
    sortable: true,
    flex: 1,
  },
  {
    headerName: 'Location',
    field: 'location',
    hide: false,
    sortable: true,
    flex: 1,
  },
  {
    headerName: 'Occurrence',
    field: 'occurrenceName',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
  {
    headerName: 'Plant Area',
    field: 'geospatialObject',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
  {
    headerName: 'Phase',
    field: 'stageName',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
  {
    headerName: 'Year',
    field: 'experimentYear',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
  {
    headerName: 'Design',
    field: 'experimentDesignType',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
  {
    headerName: 'Entry Type (*)',
    field: 'entryType',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
  {
    headerName: 'N. Entries',
    field: 'entryCount',
    hide: false,
    sortable: true,
    flex: 0.6,
  },
  {
    headerName: 'Harvest (*)',
    field: 'harvestStatus',
    hide: false,
    sortable: true,
    flex: 0.8,
  },
]
