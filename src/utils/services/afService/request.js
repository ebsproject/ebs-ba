import logger from 'utils/logger';
import { requestDummy } from '../dummyData';
import clientAxios from 'utils/clientAxiosAF';
import { getUrlAFSaveNewRequest } from 'utils/helpers/afHelper';

export async function getRequestList(limit, page) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalElements: 0,
      },
    },
  };
  try {
    dataSet.data = requestDummy;
    dataSet.metadata.pagination.totalPages = 0;
    dataSet.metadata.pagination.totalElements = requestDummy.length;
    dataSet.status.status = 200;
  } catch (ex) {
    console.log(`Error axios - Get Request data:`.toString(), ex);
    logger.push(JSON.stringify({ type: 'error', message: ex }));
    dataSet.status.status = ex.response.status;
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view experiment locations.';
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No experiment locations found.';
    } else if (500 <= ex.response.status) {
      dataSet.status.message = 'Unable to view experiment locations.';
    }
  }
  return dataSet;
}

export function createNewRequestData({
  dataSourceUrl,
  token,
  experiments,
  responseVariableTraits,
  analysisObjective,
  traitAnalysisPattern,
  experimentLocationAnalysisPattern,
  analysisConfiguration,
  spatialAdjusting,
  prediction,
}) {
  const newRequestData = {
    dataSource: 'EBS',
    dataSourceUrl: dataSourceUrl,
    dataSourceAccessToken: token,
    crop: 'string',
    institute: 'string',
    analysisType: 'ANALYZE',
    experimentIds: [...new Set(experiments.map((exp) => exp.experimentDbId))],
    occurrenceIds: experiments.map((exp) => exp.occurrenceDbId),
    traitIds: responseVariableTraits.map((trai) => trai.id),
    analysisObjectivePropertyId: analysisObjective,
    analysisConfigPropertyId: traitAnalysisPattern,
    expLocAnalysisPatternPropertyId: experimentLocationAnalysisPattern,
    configFormulaPropertyId: analysisConfiguration,
    configResidualPropertyId: spatialAdjusting,
    predictionId: prediction,
  };
  return newRequestData;
}

export async function postSaveNewRequest({ newRequestData }) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
      },
    },
  };
  const parameter = newRequestData;
  try {
    let url = getUrlAFSaveNewRequest();
    let result = await clientAxios.post(url, parameter);
    dataSet.status.status = 201;
  } catch (ex) {
    console.log(`Error axios - save new request POST:`.toString(), ex);
    logger.push(JSON.stringify({ type: 'error', message: ex }));
    dataSet.status.status = ex.request.status;
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view experiment locations.';
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No experiment locations found.';
    } else if (500 <= ex.response.status) {
      dataSet.status.message = `Unable create new request: ${ex.request.text}`;
    }
  }
  return dataSet;
}

export const getRequestListColumns = [
  {
    Header: 'Experiment',
    accessor: 'experiment',
    filter: true,
    hidden: false,
  },
  {
    Header: 'Status',
    accessor: 'status',
    filter: true,
    hidden: true,
  },
  {
    Header: 'Status',
    accessor: 'statusButton',
    filter: true,
    hidden: false,
  },
  {
    Header: 'Request ID',
    accessor: 'requestId',
    filter: true,
    hidden: false,
  },
  {
    Header: 'Requester',
    accessor: 'requester',
    filter: true,
    hidden: false,
  },
  {
    Header: 'Timestamp',
    accessor: 'timestamp',
    filter: true,
    hidden: false,
  },
  { Header: 'Objective', accessor: 'objective', filter: true, hidden: false },
  { Header: 'Model', accessor: 'model', filter: true, hidden: false },
  { Header: 'Location', accessor: 'location', filter: true, hidden: false },
  { Header: 'Traits', accessor: 'trait', filter: true, hidden: false },
];
