import clientAxios from 'utils/clientAxiosAF'
import {
  AF_PROPERTIES_EXPERIMENT_LOCATION_ANALYSIS_PATTERN,
  GetUrlAFPropertiesSearch,
} from 'utils/helpers/afHelper'
import logger from 'utils/logger'

export async function getExperimentLocationAnalysisPattern(occurrences) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  }
  const parameter = {}
  try {
    let url = GetUrlAFPropertiesSearch({
      propertyRoot: AF_PROPERTIES_EXPERIMENT_LOCATION_ANALYSIS_PATTERN,
    })
    let result = await clientAxios.get(url, parameter)
    dataSet.data = result.data.result.data
    //dataSet.metadata.pagination.totalPages =
    //  result.data.metadata.pagination.totalPages
    //dataSet.metadata.pagination.totalCount =
    //  result.data.metadata.pagination.totalCount
    /*
    if (occurrences.length === 1) {
      dataSet.data = dataSet.data.filter((item) => {
        if (
          item.propertyId ===
          AF_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_UNIC_EXPERIMNET_ID
        ) {
          return item
        }
      })
    }
    */
    dataSet.status.status = 200
  } catch (ex) {
    console.log(`Error axios - Experiment location analisys:`.toString(), ex)
    logger.push(JSON.stringify({ type: 'error', message: ex }))
    dataSet.status.status = ex.response.status
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view experiments.'
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No experiments found.'
    } else if (500 <= ex.response.status) {
      dataSet.status.message = 'Unable to view experiments.'
    }
  }
  return dataSet
}
