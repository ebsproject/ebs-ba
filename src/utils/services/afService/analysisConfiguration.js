import clientAxios from 'utils/clientAxiosAF'
import logger from 'utils/logger'
import {
  getExperimentOrLocationFromExperimentLocationAnalysisPattern,
  getUrlAFAnalysisConfigurationSearch,
} from 'utils/helpers/afHelper'

export async function getAnalysisConfiguration(
  traitAnalysisPattern,
  experimentLocationAnalysisPattern,
) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  }
  const parameter = {}
  try {
    const traitPattern = JSON.parse(traitAnalysisPattern).label.toLowerCase()
    const expAnalysisPattern = getExperimentOrLocationFromExperimentLocationAnalysisPattern(
      experimentLocationAnalysisPattern,
    ).experiment
    const locAnalysisPattern = getExperimentOrLocationFromExperimentLocationAnalysisPattern(
      experimentLocationAnalysisPattern,
    ).location
    let url = getUrlAFAnalysisConfigurationSearch({
      traitPattern: traitPattern,
      expAnalysisPattern: expAnalysisPattern,
      locAnalysisPattern: locAnalysisPattern,
    })
    let result = await clientAxios.get(url, parameter)
    dataSet.data = result.data.result.data
    //dataSet.metadata.pagination.totalPages =
    //  result.data.metadata.pagination.totalPages
    //dataSet.metadata.pagination.totalCount =
    //  result.data.metadata.pagination.totalCount
    dataSet.status.status = 200
  } catch (ex) {
    console.log(`Error axios - Analysis configuration:`.toString(), ex)
    logger.push(JSON.stringify({ type: 'error', message: ex }))
    dataSet.status.status = ex.response.status
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view experiments.'
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No experiments found.'
    } else if (500 <= ex.response.status) {
      dataSet.status.message = 'Unable to view experiments.'
    }
  }
  return dataSet
}
