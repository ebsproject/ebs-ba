import clientAxios from 'utils/clientAxiosAF'
import {
  AF_PROPERTIES_ANALYSIS_CONFIGURATION_DETAIL_SPACIALADJUSTING,
  getUrlAFAnalysisConfigurationDetail,
} from 'utils/helpers/afHelper'
import logger from 'utils/logger'

export async function getSpatialAdjusting(occurrences, analysisConfiguration) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  }
  const parameter = {}
  try {
    let url = getUrlAFAnalysisConfigurationDetail({
      analysisConfigurationId: analysisConfiguration,
      analysisConfigurationDetail: AF_PROPERTIES_ANALYSIS_CONFIGURATION_DETAIL_SPACIALADJUSTING,
    })
    let result = await clientAxios.get(url, parameter)
    //console.log(result)
    dataSet.data = result.data.result.data
    //dataSet.metadata.pagination.totalPages =
    //  result.data.metadata.pagination.totalPages
    //dataSet.metadata.pagination.totalCount =
    //  result.data.metadata.pagination.totalCount
    dataSet.status.status = 200
  } catch (ex) {
    console.log(`Error axios - Mian Model:`.toString(), ex)
    logger.push(JSON.stringify({ type: 'error', message: ex }))
    dataSet.status.status = ex.response.status
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view experiments.'
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No experiments found.'
    } else if (500 <= ex.response.status) {
      dataSet.status.message = 'Unable to view experiments.'
    }
  }
  return dataSet
}
