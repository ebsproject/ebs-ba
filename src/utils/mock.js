import AxiosMockAdapter from 'axios-mock-adapter'
import clientAxiosMock from './clientAxiosMock'
const instance = new AxiosMockAdapter(clientAxiosMock, { delayResponse: 100 })
export default instance
