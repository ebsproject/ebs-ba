export function getAtributeFromJsonString(
  jsonString,
  attribute,
  defaultResult = '',
) {
  let result = defaultResult
  if (jsonString !== null && jsonString !== '') {
    try {
      let json = JSON.parse(jsonString)
      result = json[attribute]
    } catch (ex) {
      console.log(`Error to convert string in json: ${jsonString}`)
    }
  }
  return result
}
