export const STATE_REQUEST_DRAFT = 'Draft'
export const STATE_REQUEST_PROCESSING = 'Processing'
export const STATE_REQUEST_COMPLETE = 'Complete'
export const SORT_EXPERIMENTDBID_DESC = 'experimentDbId:desc'
export const SORT_OCCURRENCEDBID_DESC = 'occurrenceDbId:desc'

export function getUrlB4rOccurrencesSearch(limit = -1, page = -1, sort = '') {
  let parameters = new Array()
  let url = 'occurrences-search'

  if (limit !== -1) {
    parameters.push(`limit=${limit}`.toString())
  }
  if (page !== -1) {
    parameters.push(`page=${page}`.toString())
  }
  if (sort !== '') {
    parameters.push(`sort=${sort}`.toString())
  }
  if (parameters.length > 0) {
    url += '?' + parameters.join('&')
  }
  return url
}

export function getUrlB4rOccurrencesPlotDataTableSearch(ocurrenceDbId) {
  let url = `occurrences/${ocurrenceDbId}/plot-data-table-search`.toString()
  return url
}

export function getUrlB4rOccurrencesPlotDataSearch(ocurrenceDbId) {
  let url = `occurrences/${ocurrenceDbId}/plot-data-search`.toString()
  return url
}

export function getUrlB4rExperimentSearch(
  limit,
  page,
  sort = 'experimentDbId:desc',
) {
  let url = `experiments-search?limit=${limit}&page=${page}&sort=${sort}`.toString()
  return url
}
