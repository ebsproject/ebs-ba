export const AF_PROPERTIES_ANALYSIS_OBJECTIVE = 'objective'
export const AF_PROPERTIES_ANALYSIS_PATTERN = 'trait_pattern'
export const AF_PROPERTIES_EXPERIMENT_LOCATION_ANALYSIS_PATTERN =
  'exptloc_analysis_pattern'
export const AF_PROPERTIES_ANALYSIS_CONFIGURATION_DETAIL_MAINMODEL = 'formulas'
export const AF_PROPERTIES_ANALYSIS_CONFIGURATION_DETAIL_SPACIALADJUSTING =
  'residuals'
export const AF_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_UNIC_EXPERIMNET_ID = 'string'
const ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINLGE_LOCATION = 8
const ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_MULTI_LOCATION = 9
const ID_PREDICTION_ELAP_SINLGE_LOCATION = 19
const ID_PREDICTION_ELAP_MULTI_LOCATION = 20

export function getExperimentOrLocationFromExperimentLocationAnalysisPattern(
  experimentLocationAnalysisPatternJsonString,
) {
  let result = {
    experiment: '',
    location: '',
  }
  if (experimentLocationAnalysisPatternJsonString != '') {
    result.experiment = JSON.parse(experimentLocationAnalysisPatternJsonString)
      .label.toLowerCase()
      .trim()
      .split('-')[0]
      .trim()
      .split(' ')[0]
    const experiment = JSON.parse(experimentLocationAnalysisPatternJsonString)
      .label.toLowerCase()
      .trim()
      .split('-')[1]
      .trim()
      .split(' ')[0]
    result.location = experiment === 'multiple' ? 'multi' : experiment
  }
  return result
}

export function getPredictionByExpLoc(experimentLocationAnalysisPattern) {
  //adding Prediction The interface should create it, If exploc_pattern is single location the prediction is G (property_id 19), if the exploc_pattern is multi the prediction is GxE (property_id 20).
  const location = getExperimentOrLocationFromExperimentLocationAnalysisPattern(
    experimentLocationAnalysisPattern,
  ).location
  return location === 'single'
    ? ID_PREDICTION_ELAP_SINLGE_LOCATION
    : ID_PREDICTION_ELAP_MULTI_LOCATION
}

export function GetUrlAFPropertiesSearch({
  propertyRoot,
  page = 0,
  pageSize = 100,
  isActive = true,
}) {
  let url = `properties?propertyRoot=${propertyRoot}&isActive=${isActive}&page=${page}&pageSize=${pageSize}`.toString()
  return url
}

export function getUrlAFAnalysisConfigurationSearch({
  traitPattern,
  expAnalysisPattern,
  locAnalysisPattern,
  page = 0,
  pageSize = 100,
}) {
  let parameters = new Array()
  let url = 'analysis-configs'
  if (traitPattern !== '') {
    parameters.push(`traitPattern=${traitPattern}`.toString())
  }
  if (expAnalysisPattern !== '') {
    parameters.push(`expAnalysisPattern=${expAnalysisPattern}`.toString())
  }
  if (locAnalysisPattern !== '') {
    parameters.push(`locAnalysisPattern=${locAnalysisPattern}`.toString())
  }
  if (page !== '') {
    parameters.push(`page=${page}`.toString())
  }
  if (pageSize !== '') {
    parameters.push(`pageSize=${pageSize}`.toString())
  }
  if (parameters.length > 0) {
    url += '?' + parameters.join('&')
  }
  return url
}

export function getUrlAFAnalysisConfigurationDetail({
  analysisConfigurationId,
  analysisConfigurationDetail,
}) {
  let url = `analysis-configs/${analysisConfigurationId}/${analysisConfigurationDetail}`.toString()
  return url
}

export function getUrlAFSaveNewRequest() {
  let url = `requests`.toString()
  return url
}
