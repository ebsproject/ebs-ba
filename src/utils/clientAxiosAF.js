import Axios from 'axios';

const accessToken = localStorage.getItem('id_token');
const instance = Axios.create({
  baseURL: 'http://baf.gobii.org/v1/',
  headers: {
    //Authorization: `Bearer ${accessToken}`,
  },
});
export default instance;
