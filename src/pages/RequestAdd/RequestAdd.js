import React from 'react';
import AddRequest from 'components/ui/templates/AddRequest';
import {
  ADD_ANALISYSC_ACTION,
  ADD_ANALISYSO_ACTION,
  ADD_EXPERIMENTLAP_ACTION,
  ADD_EXPERIMENT_ACTION,
  ADD_MAINM_ACTION,
  ADD_PREDICTION_ACTION,
  ADD_REMOVE_EXPERIMENT_ACTION,
  ADD_RESPONSEVT_ACTION,
  ADD_SPATIALA_ACTION,
  ADD_TRAITAP_ACTION,
  NEW_REQUEST_ACTION,
} from 'reducks/modules/NewRequest';
import {
  getIntersectionTraitFromOccurrences,
  getOccurrenceWithTraitByExperimentDbId,
  occurenceColumns,
  intersectionTrait,
} from 'utils/services/b4RService/occurrence';
import { getAnalysisObjective } from 'utils/services/afService/analysisObjective';
import { getTraitAnalysisPattern } from 'utils/services/afService/traitAnalysisPattern';
import { getExperimentLocationAnalysisPattern } from 'utils/services/afService/experimentLocationAnalysisPattern';
import { getAnalysisConfiguration } from 'utils/services/afService/analysisConfiguration';
import { getMainModel } from 'utils/services/afService/mainModel';
import { getPrediction } from 'utils/services/b4RService/default';
import { getSpatialAdjusting } from 'utils/services/afService/spacialAdjusting';
import { ROUTE_LIST_REQUEST } from 'utils/configDefault';
import {
  getExperiment,
  experimentColumns,
} from 'utils/services/b4RService/experiment';
import {
  createNewRequestData,
  postSaveNewRequest,
} from 'utils/services/afService/request';
import { getPredictionByExpLoc } from 'utils/helpers/afHelper';
import { SMclient, FIND_REQUEST_LIST } from 'utils/SMclient';

export default function RequestAdd() {
  const titleData = {
    title: 'Analytics Reques Manager',
    subtitle: 'Add New Request',
    note: '',
  };
  const selects = {
    responseVariableTrait: {
      responseVariableTrait: [],
      functionData: getIntersectionTraitFromOccurrences,
      accionSave: ADD_RESPONSEVT_ACTION,
    },
    analysisObjective: {
      analysisObjective: '',
      functionData: getAnalysisObjective,
      title: 'Analysis objective',
      id: 'ao1',
      accionSave: ADD_ANALISYSO_ACTION,
    },
    traitAnalysisPattern: {
      traitAnalysisPattern: '',
      functionData: getTraitAnalysisPattern,
      title: 'Trait Analysis Pattern',
      id: 'tap1',
      accionSave: ADD_TRAITAP_ACTION,
    },
    experimentLocationAnalysisPattern: {
      experimentLocationAnalysisPattern: '',
      functionData: getExperimentLocationAnalysisPattern,
      title: 'Experiment Location Analysis Pattern',
      id: 'elap1',
      accionSave: ADD_EXPERIMENTLAP_ACTION,
    },
    analysisConfiguration: {
      analysisConfiguration: '',
      functionData: getAnalysisConfiguration,
      title: 'Analysis Configuration',
      id: 'ac1',
      accionSave: ADD_ANALISYSC_ACTION,
    },
    mainModel: {
      mainModel: '',
      functionData: getMainModel,
      title: 'Main Model',
      id: 'mm1',
      accionSave: ADD_MAINM_ACTION,
    },
    prediction: {
      prediction: '',
      functionData: getPrediction,
      title: 'Prediction',
      id: 'p1',
      accionSave: ADD_PREDICTION_ACTION,
    },
    spatialAdjusting: {
      spatialAdjusting: '',
      functionData: getSpatialAdjusting,
      title: 'Spatial Adjusting',
      id: 'sp1',
      accionSave: ADD_SPATIALA_ACTION,
    },
  };
  const setting = {
    tokenHelpPopUp: true,
    action: {
      NEW_REQUEST_ACTION: NEW_REQUEST_ACTION,
    },
    routes: {
      rotuelist: ROUTE_LIST_REQUEST,
    },
  };
  const experiment = {
    functionData: getExperiment,
    experiment: [],
    columns: experimentColumns,
    functionDataDetails: getOccurrenceWithTraitByExperimentDbId,
    columnsDetails: occurenceColumns,
    accionSave: ADD_EXPERIMENT_ACTION,
    accionAddRemove: ADD_REMOVE_EXPERIMENT_ACTION,
    functionGetTrait: intersectionTrait,
  };
  const request = {
    functionSaveRequest: postSaveNewRequest,
    dataSourceUrl: 'https://cbapi-dev.ebsproject.org/v3/',
    functionCreateNewRequest: createNewRequestData,
    functionGetPrediction: getPredictionByExpLoc,
  };

  React.useEffect(() => {
    SMclient.query({
      query: FIND_REQUEST_LIST,
      variables: {
        page: 1,
        size: 10,
      },
      fetchPolicy: 'no-cache',
    })
      .then(({ data }) => {
        console.log(data);
      })
      .catch(({ message }) => {
        console.log(message);
      });
  }, []);
  console.log(process.env)
  return (
    <AddRequest
      setting={setting}
      titleData={titleData}
      selects={selects}
      experiment={experiment}
      request={request}
    />
  );
}
RequestAdd.prototype = {};
