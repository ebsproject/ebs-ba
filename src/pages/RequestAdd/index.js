import { connect } from 'react-redux'
import RequestAdd from './RequestAdd'

export default connect(
  (state) => ({
    newRequest: state.newRequest,
  }),
  {},
)(RequestAdd)
