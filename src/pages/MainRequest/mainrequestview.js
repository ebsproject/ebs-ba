import React from 'react';
import PropTypes from 'prop-types';
import Container from '@material-ui/core/Container';
import MainRequestTemplate from 'components/ui/templates/MainRequest';
import {
  getRequestList,
  getRequestListColumns,
} from 'utils/services/afService/request';
import { NEW_REQUEST_ACTION } from 'reducks/modules/NewRequest';
import { ROUTE_ADD_REQUEST } from 'utils/configDefault';
import {
  STATE_REQUEST_COMPLETE,
  STATE_REQUEST_PROCESSING,
  STATE_REQUEST_DRAFT,
} from 'utils/helpers/b4rHelper';

export default function MainRequestView(props) {
  // Props
  const { ...rest } = props;
  const navBar = {
    buttons: [
      {
        id: 'getTokenId',
        label: 'Get Token',
        active: true,
        parameter: {
          idStorage: process.env.REACT_APP_TOKEN_STORAGE_LOCATION,
          uriParent: 'https://cbapi-dev.ebsproject.org/v3/',
        },
      },
    ],
  };

  const requestList = {
    functionData: getRequestList,
    columnsData: getRequestListColumns,
    title: 'List of Job created',
    buttons: [
      {
        id: 'newRequest',
        title: 'ADD NEW REQUEST',
        color: 'inherit',
        parameters: {
          actionSave: NEW_REQUEST_ACTION,
          ruteToPage: ROUTE_ADD_REQUEST,
        },
      },
      {
        id: 'removeRequest',
        title: 'REMOVE REQUEST',
        color: 'inherit',
        parameters: {
          actionSave: '',
        },
      },
    ],
    stateRequest: {
      complete: STATE_REQUEST_COMPLETE,
      processing: STATE_REQUEST_PROCESSING,
      draft: STATE_REQUEST_DRAFT,
    },
  };
  /*
  @prop data-testid: Id to use inside mainrequest.test.js file.
 */
  return (
    <Container data-testid={'MainRequestTestId'} component='main' maxWidth={'xl'}>
      <MainRequestTemplate navbar={navBar} requestlist={requestList} />
    </Container>
  );
}
// Type and required properties
MainRequestView.propTypes = {};
// Default properties
MainRequestView.defaultProps = {};
