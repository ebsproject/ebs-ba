import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import { FormControl, MenuItem, Select, Typography } from '@material-ui/core'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ObjectSelectAtom = React.forwardRef((props, ref) => {
  const {
    children,
    idselect,
    valuedisplay,
    handlechange,
    messageerror,
    valuedisplays,
    title,
    valueid,
    valuelabel,
    classes,
    ...rest
  } = props

  const keyForm = `form_create_new_request_obj_${idselect}`.toString()
  return (
    /* 
     @prop data-testid: Id to use inside objectselect.test.js file.
     */
    <div data-testid={'ObjectSelectTestId'} ref={ref}>
      <Typography variant='body1'>{title}</Typography>
      <FormControl variant='outlined' key={keyForm} size='small'>
        <Select
          id={idselect}
          value={valuedisplay === null ? '' : valuedisplay}
          fullWidth
          onChange={handlechange}
          helpertext={messageerror}
          className={classes.select}
        >
          {valuedisplays.map((value, index) => {
            const key = `${value[valueid]}_${idselect}_${index}`.toString()
            const valueToMenu = JSON.stringify({
              id: value[valueid],
              label: value[valuelabel],
            })
            return (
              <MenuItem value={valueToMenu} key={key}>
                {value[valuelabel]}
              </MenuItem>
            )
          })}
        </Select>
      </FormControl>
    </div>
  )
})
// Type and required properties
ObjectSelectAtom.propTypes = {
  idselect: PropTypes.string.isRequired,
  valuedisplay: PropTypes.string.isRequired,
  handlechange: PropTypes.func.isRequired,
  messageerror: PropTypes.string.isRequired,
  valuedisplays: PropTypes.array.isRequired,
  valueid: PropTypes.string.isRequired,
  valuelabel: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
ObjectSelectAtom.defaultProps = {}

export default ObjectSelectAtom
