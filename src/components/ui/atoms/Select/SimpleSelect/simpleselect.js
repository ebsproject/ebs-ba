import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import { FormControl, MenuItem, Select, Typography } from '@material-ui/core'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const SimpleSelectAtom = React.forwardRef((props, ref) => {
  const {
    children,
    idselect,
    valuedisplay,
    handlechange,
    messageerror,
    valuedisplays,
    title,
    valueid,
    valuelabel,
    classes,
    ...rest
  } = props

  const keyForm = `form_create_new_request_${idselect}`.toString()
  return (
    /* 
     @prop data-testid: Id to use inside simpleselect.test.js file.
     */
    <div className={classes.formSelect} data-testid={'SimpleSelectTestId'} ref={ref}>
      <Typography variant='body1'>{title}</Typography>
      <FormControl variant='outlined' key={keyForm} size='small'>
        <Select
          id={idselect}
          value={valuedisplay === 0 ? '' : valuedisplay}
          fullWidth
          onChange={handlechange}
          helpertext={messageerror}
          className={classes.select}
        >
          {valuedisplays.map((value, index) => {
            const key = `${value[valueid]}_${idselect}_${index}`.toString()
            return (
              <MenuItem value={value[valueid]} key={key}>
                {value[valuelabel]}
              </MenuItem>
            )
          })}
        </Select>
      </FormControl>
    </div>
  )
})
// Type and required properties
SimpleSelectAtom.propTypes = {
  idselect: PropTypes.string.isRequired,
  //valuedisplay: PropTypes.number,
  handlechange: PropTypes.func.isRequired,
  messageerror: PropTypes.string.isRequired,
  valuedisplays: PropTypes.array.isRequired,
  valueid: PropTypes.string.isRequired,
  valuelabel: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
SimpleSelectAtom.defaultProps = {}

export default SimpleSelectAtom
