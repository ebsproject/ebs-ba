import React, { useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import { Button, Grid, ListItemIcon, ListItemText, Switch } from '@material-ui/core'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ItemCheckAtom = React.forwardRef((props, ref) => {
  const { children, label, ischeck, classes, ...rest } = props
  const [isCheck, setIsCheck] = useState(ischeck)

  const handelCheck = () => {
    setIsCheck((check) => !check)
  }

  return (
    /* 
     @prop data-testid: Id to use inside itemcheck.test.js file.
     */
    <div data-testid={'ItemCheckTestId'} ref={ref}>
      <Grid container spacing={10} alignContent='flex-start' justify='flex-start'>
        <Grid item xs={8}>
          <ListItemText primary={label} />
        </Grid>
        <Grid item xs={2} justify='flex-end'>
          <ListItemIcon>
            <Switch checked={isCheck} onChange={handelCheck} />
          </ListItemIcon>
        </Grid>
      </Grid>
    </div>
  )
})
// Type and required properties
ItemCheckAtom.propTypes = {
  label: PropTypes.string.isRequired,
  ischeck: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
ItemCheckAtom.defaultProps = {}

export default ItemCheckAtom
