import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Box, Grid, Typography } from '@material-ui/core'
import { Alert } from '@material-ui/lab'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const SubTitleMolecule = React.forwardRef((props, ref) => {
  const { children, titleText, message, severity, ...rest } = props

  return (
    /* 
     @prop data-testid: Id to use inside subtitle.test.js file.
     */
    <Grid
      container
      ref={ref}
      data-testid={'SubTitleTestId'}
      justify={'flex-start'}
      spacing={3}
    >
      <Grid item>
        <Typography variant='h4'>{titleText}</Typography>
      </Grid>
      <Grid item>
        {message !== '' ? <Alert severity={severity}>{message}</Alert> : null}
      </Grid>
    </Grid>
  )
})
// Type and required properties
SubTitleMolecule.propTypes = {
  titleText: PropTypes.string.isRequired,
  severity: PropTypes.string,
  message: PropTypes.string,
}
// Default properties
SubTitleMolecule.defaultProps = {
  titleText: '',
}

export default SubTitleMolecule
