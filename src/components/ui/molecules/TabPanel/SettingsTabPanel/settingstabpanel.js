import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Collapse, Grid } from '@material-ui/core'
import TraitAutoSelectMolecule from '../../Select/TraitAutoSelect'
import AutoSelectMolecule from '../../Select/AutoSelect'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const SettingsTabPanelMolecule = React.forwardRef((props, ref) => {
  const { children, value, index, selects, experiment, classes, ...rest } = props
  const isIn = value === index

  return (
    /* 
     @prop data-testid: Id to use inside settingstabpanel.test.js file.
     */
    <Grid container ref={ref} data-testid={'SettingsTabPanelTestId'}>
      <Grid item xs={1}></Grid>
      <Grid item xs={11}>
        <Collapse in={isIn} timeout='auto' unmountOnExit>
          <TraitAutoSelectMolecule
            actionSave={selects.responseVariableTrait.accionSave}
            responseVariableTrait={
              selects.responseVariableTrait.responseVariableTrait
            }
            functionData={selects.responseVariableTrait.functionData}
            occurrences={experiment.experiment}
          />
          <AutoSelectMolecule
            actionSave={selects.analysisObjective.accionSave}
            id={selects.analysisObjective.id}
            title={selects.analysisObjective.title}
            valueDisplay={selects.analysisObjective.analysisObjective}
            functionData={selects.analysisObjective.functionData}
            occurrences={experiment.experiment}
            classes={classes}
          />
        </Collapse>
      </Grid>
    </Grid>
  )
})
// Type and required properties
SettingsTabPanelMolecule.propTypes = {
  value: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  selects: PropTypes.object.isRequired,
  experiment: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
SettingsTabPanelMolecule.defaultProps = {}

export default SettingsTabPanelMolecule
