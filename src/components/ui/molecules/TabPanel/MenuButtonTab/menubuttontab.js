import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Box, Button, Grid, IconButton } from '@material-ui/core'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const MenuButtonTabMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    cancelAction,
    cancelRoute,
    functionSetTab,
    backDisable,
    nextDisable,
    nextText,
    tapsLength,
    newrequestdata,
    valuetab,
    routeaftersave,
    functionsave,
    recivemessage,
    ...rest
  } = props
  const dispatch = useDispatch()
  const history = useHistory()

  const handleNext = () => {
    if (valuetab + 1 === tapsLength) {
      //console.log(JSON.stringify(newrequestdata, null, 2))
      functionsave({ newRequestData: newrequestdata }).then((res) => {
        if (res.status.status === 201) {
          history.push(routeaftersave)
        } else {
          //
        }
      })
    } else {
      functionSetTab((value) => value + 1)
    }
  }
  const handleBack = () => {
    functionSetTab((value) => value - 1)
  }
  const handleCancel = () => {
    dispatch({
      type: cancelAction,
      payload: '',
    })
    functionSetTab(0)
    history.push(cancelRoute)
  }

  return (
    /* 
     @prop data-testid: Id to use inside menubuttontab.test.js file.
     */
    <Grid
      container
      ref={ref}
      direction='row'
      justify='flex-end'
      data-testid={'MenuButtonTabTestId'}
      spacing={1}
    >
      <Grid item>
        <Button onClick={handleCancel} size={'small'} variant={'contained'}>
          {'Cancel'}
        </Button>
      </Grid>
      <Grid item>
        <Button
          onClick={handleBack}
          variant={'contained'}
          size={'small'}
          color={'secondary'}
          disabled={backDisable}
        >
          {'Back'}
        </Button>
      </Grid>
      <Grid item>
        <Button
          onClick={handleNext}
          variant={'contained'}
          size={'small'}
          color={'primary'}
          disabled={nextDisable}
        >
          {nextText}
        </Button>
      </Grid>
    </Grid>
  )
})
// Type and required properties
MenuButtonTabMolecule.propTypes = {
  cancelAction: PropTypes.string.isRequired,
  cancelRoute: PropTypes.string.isRequired,
  functionSetTab: PropTypes.func.isRequired,
  backDisable: PropTypes.bool.isRequired,
  nextDisable: PropTypes.bool.isRequired,
  nextText: PropTypes.string.isRequired,
  tapsLength: PropTypes.number.isRequired,
  newrequestdata: PropTypes.object.isRequired,
  routeaftersave: PropTypes.string.isRequired,
  functionsave: PropTypes.func.isRequired,
}
// Default properties
MenuButtonTabMolecule.defaultProps = {
  backDisable: true,
  nextDisable: true,
  nextText: 'Next',
}

export default MenuButtonTabMolecule
