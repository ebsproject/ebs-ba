import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Collapse, Grid } from '@material-ui/core'
import AnalysisConfigurationAutoSelectMolecule from '../../Select/AnalysisConfigurationAutoSelect'
import AnalysisConfigurationDetailAutoSelectMolecule from '../../Select/AnalysisConfigurationDetailAutoSelect'
import AutoSelectMolecule from '../../Select/AutoSelect'
import AutoObjectSelectMolecule from '../../Select/AutoObjectSelect'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ParametersTabPanelMolecule = React.forwardRef((props, ref) => {
  const { children, value, index, selects, experiment, classes, ...rest } = props
  const isIn = value === index
  //# To habilite prediction
  /*
            <AutoSelectMolecule
              actionSave={selects.prediction.accionSave}
              id={selects.prediction.id}
              title={selects.prediction.title}
              valueDisplay={selects.prediction.prediction}
              functionData={selects.prediction.functionData}
              occurrences={experiment.experiment}
            />
  */

  return (
    /* 
     @prop data-testid: Id to use inside parameterstabpanel.test.js file.
     */
    <Grid container ref={ref} data-testid={'ParametersTabPanelTestId'}>
      <Grid item xs={1}></Grid>
      <Grid item xs={11}>
        <Collapse in={isIn} timeout='auto' unmountOnExit>
          <Grid container>
            <AutoObjectSelectMolecule
              actionSave={selects.traitAnalysisPattern.accionSave}
              id={selects.traitAnalysisPattern.id}
              title={selects.traitAnalysisPattern.title}
              valueDisplay={selects.traitAnalysisPattern.traitAnalysisPattern}
              functionData={selects.traitAnalysisPattern.functionData}
              occurrences={experiment.experiment}
              classes={classes}
            />
          </Grid>
          <AutoObjectSelectMolecule
            actionSave={selects.experimentLocationAnalysisPattern.accionSave}
            id={selects.experimentLocationAnalysisPattern.id}
            title={selects.experimentLocationAnalysisPattern.title}
            valueDisplay={
              selects.experimentLocationAnalysisPattern
                .experimentLocationAnalysisPattern
            }
            functionData={selects.experimentLocationAnalysisPattern.functionData}
            occurrences={experiment.experiment}
            classes={classes}
          />
          <AnalysisConfigurationAutoSelectMolecule
            actionSave={selects.analysisConfiguration.accionSave}
            id={selects.analysisConfiguration.id}
            title={selects.analysisConfiguration.title}
            valueDisplay={selects.analysisConfiguration.analysisConfiguration}
            functionData={selects.analysisConfiguration.functionData}
            occurrences={experiment.experiment}
            experimentLocationAnalysisPattern={
              selects.experimentLocationAnalysisPattern
                .experimentLocationAnalysisPattern
            }
            traitAnalysisPattern={selects.traitAnalysisPattern.traitAnalysisPattern}
            analysisObjective={selects.analysisObjective.analysisObjective}
            classes={classes}
          />
          <AnalysisConfigurationDetailAutoSelectMolecule
            actionSave={selects.mainModel.accionSave}
            id={`${selects.mainModel.id}_mainmodel`}
            title={selects.mainModel.title}
            valueDisplay={selects.mainModel.mainModel}
            functionData={selects.mainModel.functionData}
            occurrences={experiment.experiment}
            analysisConfiguration={
              selects.analysisConfiguration.analysisConfiguration
            }
            classes={classes}
          />
          <AnalysisConfigurationDetailAutoSelectMolecule
            actionSave={selects.spatialAdjusting.accionSave}
            id={`${selects.spatialAdjusting.id}_spacial`}
            title={selects.spatialAdjusting.title}
            valueDisplay={selects.spatialAdjusting.spatialAdjusting}
            functionData={selects.spatialAdjusting.functionData}
            occurrences={experiment.experiment}
            analysisConfiguration={
              selects.analysisConfiguration.analysisConfiguration
            }
            classes={classes}
          />
        </Collapse>
      </Grid>
    </Grid>
  )
})
// Type and required properties
ParametersTabPanelMolecule.propTypes = {
  value: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  selects: PropTypes.object.isRequired,
  experiment: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
ParametersTabPanelMolecule.defaultProps = {}

export default ParametersTabPanelMolecule
