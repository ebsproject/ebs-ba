import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import ParametersTabPanel from './parameterstabpanel'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ParametersTabPanel></ParametersTabPanel>, div)
})

// Props to send component to be rendered
const props = {}

test('Render correctly', () => {
  const { getByTestId } = render(
    <ParametersTabPanel {...props}></ParametersTabPanel>,
  )
  expect(getByTestId('ParametersTabPanelTestId')).toBeInTheDocument()
})
