import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid, Collapse } from '@material-ui/core'
import TableMDMolecule from '../../Table/MasterDetail/TableMD'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ExperimentsTabPanelMolecule = React.forwardRef((props, ref) => {
  const { children, value, index, experiment, classes, ...rest } = props
  const isIn = value === index

  return (
    /* 
     @prop data-testid: Id to use inside experimentstabpanel.test.js file.
     */
    <Grid container ref={ref} data-testid={'ExperimentsTabPanelTestId'}>
      <Grid item xs={12}>
        <Collapse in={isIn} timeout='auto' unmountOnExit>
          <TableMDMolecule
            functionData={experiment.functionData}
            columns={experiment.columns}
            accionSave={experiment.accionAddRemove}
            title='Select experiment occurrences for analysis'
            functionDataDetails={experiment.functionDataDetails}
            columnsDetails={experiment.columnsDetails}
            functionGetTrait={experiment.functionGetTrait}
            classes={classes}
          />
        </Collapse>
      </Grid>
    </Grid>
  )
})
// Type and required properties
ExperimentsTabPanelMolecule.propTypes = {
  value: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  experiment: PropTypes.object.isRequired,
}
// Default properties
ExperimentsTabPanelMolecule.defaultProps = {}

export default ExperimentsTabPanelMolecule
