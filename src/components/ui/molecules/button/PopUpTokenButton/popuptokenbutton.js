import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  TextField,
  Typography,
} from '@material-ui/core'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PopUpTokenButtonMolecule = React.forwardRef((props, ref) => {
  const { children, id, label, idstorage, uriparent, ...rest } = props
  const [open, setOpen] = React.useState(false)
  const [token, setToken] = React.useState('')
  const [save, setSave] = React.useState(true)
  const [message, setMessage] = React.useState('')
  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const handleSave = () => {
    setOpen(false)
    try {
      let tokenJson = JSON.parse(token)
      if (tokenJson.result.data[0].token !== undefined) {
        localStorage.setItem(idstorage, tokenJson.result.data[0].token)
        setOpen(false)
      }
    } catch (ex) {
      console.log(ex)
    }
  }
  const handelChangeText = (event) => {
    try {
      let tokenJson = JSON.parse(event.target.value)
      if (tokenJson.result.data[0].token !== undefined) {
        setSave(false)
        setMessage('')
      } else {
        setMessage('Token not valid')
      }
    } catch (ex) {
      setMessage('Token not valid')
      console.log(ex)
    }
    setToken(event.target.value)
  }
  const handelOpenUlrToken = (event) => {
    event.preventDefault()
    window.open(`${uriparent}auth/login`.toString())
  }
  return (
    /* 
     @prop data-testid: Id to use inside popuptokenbutton.test.js file.
     */
    <div ref={ref} data-testid={'PopUpTokenButtonTestId'}>
      <Box p={1} key={`box_get_token_${id}`}>
        <IconButton
          key={`icon_butonn_get_token_${id}`}
          title={label}
          color='inherit'
          onClick={handleClickOpen}
        >
          <VpnKeyIcon />
        </IconButton>
      </Box>
      <Dialog
        key='dialog_popuptoken_main'
        open={open}
        onClose={handleClose}
        aria-labelledby='form-dialog-title'
      >
        <DialogTitle key='form-dialog-title'>Help</DialogTitle>
        <DialogContent>
          <DialogContentText key='dialogo_contex_get_token'>
            Please copy and paste the token
            <Button onClick={handelOpenUlrToken}>here</Button>
          </DialogContentText>
          <TextField
            id='tokenTextField'
            multiline
            rows={4}
            label='Token'
            fullWidth
            autoFocus
            value={token}
            onChange={handelChangeText}
            key='input_popuptoken_gettoken'
          />
        </DialogContent>
        <DialogActions>
          <Button key='cancel_button' onClick={handleClose} color='secondary'>
            Cancel
          </Button>
          <Button
            key='save_button'
            onClick={handleSave}
            color='primary'
            disabled={save}
          >
            Save
          </Button>
          <Typography variant='overline' color='error'>
            {message}
          </Typography>
        </DialogActions>
      </Dialog>
    </div>
  )
})
// Type and required properties
PopUpTokenButtonMolecule.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  idstorage: PropTypes.string.isRequired,
  uriparent: PropTypes.string.isRequired,
}
// Default properties
PopUpTokenButtonMolecule.defaultProps = {}

export default PopUpTokenButtonMolecule
