import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { AppBar, Typography } from '@material-ui/core'
import PropTypes from 'prop-types'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
}))

export default function NavBar({ title }) {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <AppBar position='static'>
        <Typography variant='h5' color='inherit'>
          {title}
        </Typography>
      </AppBar>
    </div>
  )
}

NavBar.prototype = {
  title: PropTypes.string,
}
