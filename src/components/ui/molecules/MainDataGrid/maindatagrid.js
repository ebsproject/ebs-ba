import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Box, Grid } from '@material-ui/core';
import { EbsGrid } from '@ebs/components';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const MainDataGridMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    functiondata,
    title,
    rowactions,
    columns,
    toolbaractions,
    classes,
    staterequest,
    ...rest
  } = props;
  const [data, setData] = React.useState(null);
  const [page, setPage] = React.useState(0);
  const [totalPages, setTotalPages] = React.useState(0);
  const [ttalElements, setTotalElements] = React.useState(10);

  async function fetch(page, pageSize, columnsToFilter, value) {
    let result = await functiondata(pageSize, page);
    for (let element of result.data) {
      let statusText = element.status;
      let color = classes.draft;
      if (statusText === staterequest.complete) color = classes.complete;
      if (statusText === staterequest.processing) color = classes.proccess;
      element.statusButton = (
        <Box
          className={color}
          flexWrap='nowrap'
          alignItems='center'
          textAlign='center'
        >
          {`${statusText}`.toString()}
        </Box>
      );
    }
    setData(result.data);
    setPage(page);
    setTotalPages(result.metadata.pagination.totalPages);
    setTotalElements(result.metadata.pagination.totalElements);
  }
  return (
    /* 
     @prop data-testid: Id to use inside maindatagrid.test.js file.
     */
    <Grid container ref={ref} data-testid={'MainDataGridTestId'}>
      <Grid item xs={12}>
        <EbsGrid
          toolbar={true}
          globalfilter={true}
          toolbaractions={toolbaractions}
          indexing={false}
          pagination={true}
          rowactions={rowactions}
          select='multi'
          columns={columns}
          fetch={fetch}
          data={data}
          page={page}
          totalPages={totalPages}
          totalElements={totalElements}
          title={title}
        />
      </Grid>
    </Grid>
  );
});
// Type and required properties
MainDataGridMolecule.propTypes = {
  functiondata: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  rowactions: PropTypes.func.isRequired,
  columns: PropTypes.array.isRequired,
  toolbaractions: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  staterequest: PropTypes.object.isRequired,
};
// Default properties
MainDataGridMolecule.defaultProps = {};

export default MainDataGridMolecule;
