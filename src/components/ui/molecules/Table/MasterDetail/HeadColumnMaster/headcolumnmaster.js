import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import {
  TableHead,
  TableRow,
  TableSortLabel,
  TableCell,
  TextField,
} from '@material-ui/core'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const HeadColumnMasterMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    columns,
    actionColumnWidth,
    orderBy,
    order,
    onRequestSort,
    onFilterColumn,
    columnWidth,
    classes,
    ...rest
  } = props
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  }

  const filterSortHandler = (property) => (event) => {
    onFilterColumn(event, property)
  }

  return (
    /* 
     @prop data-testid: Id to use inside headcolumnmaster.test.js file.
     */
    <TableHead>
      <TableRow>
        <TableCell style={{ width: `${actionColumnWidth}%`.toString() }} />
        {columns.map((column, index) => {
          if (!column.hide) {
            return (
              <TableCell
                key={`${index}_${column.field}`}
                style={{ width: `${columnWidth}%`.toString() }}
                sortDirection={orderBy === column.field ? order : false}
              >
                <TableSortLabel
                  active={orderBy === column.field}
                  direction={orderBy === column.field ? order : 'asc'}
                  onClick={createSortHandler(column.field)}
                >
                  {column.headerName}
                  {orderBy === column.field ? (
                    <span className={classes.visuallyHidden}>
                      {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                    </span>
                  ) : null}
                </TableSortLabel>
                <TextField
                  id={`search-input${index}_${column.field}`.toString()}
                  label='Search'
                  variant='outlined'
                  onChange={filterSortHandler(column.field)}
                  size='small'
                />
              </TableCell>
            )
          }
        })}
      </TableRow>
    </TableHead>
  )
})
// Type and required properties
HeadColumnMasterMolecule.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  onFilterColumn: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  actionColumnWidth: PropTypes.number.isRequired,
  columnWidth: PropTypes.number.isRequired,
}
// Default properties
HeadColumnMasterMolecule.defaultProps = {}

export default HeadColumnMasterMolecule
