import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import {
  Collapse,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableRow,
} from '@material-ui/core'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight'
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp'
import RowDetailMolecule from '../RowDetail'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const RowMasterMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    row,
    columns,
    columnsDetails,
    functionDataDetails,
    accionSave,
    functionGetTrait,
    numberVisibleColumn,
    columnWidth,
    actionColumnWidth,
    ...rest
  } = props
  const styleCell = {
    paddingRight: 0,
    paddingLeft: 0,
    paddingBottom: 0,
    paddingTop: 0,
    width: '100%',
  }
  const [open, setOpen] = React.useState(false)
  const [rowsDetails, setRowsDetails] = React.useState([])
  const idToSearch = row.experimentDbId

  const handelArrowOpen = () => {
    setOpen(!open)
    functionDataDetails(idToSearch).then((result) => {
      if (result.status.status === 200) {
        setRowsDetails(result.data)
      }
    })
  }
  return (
    <TableCell style={styleCell} colSpan={numberVisibleColumn + 1} ref={ref}>
      <Table size='small' aria-label='purchases'>
        <TableBody>
          <TableRow hover role='checkbox' tabIndex={-1}>
            <TableCell style={{ width: `${actionColumnWidth}%`.toString() }}>
              <IconButton
                aria-label='expand row'
                size='small'
                onClick={handelArrowOpen}
              >
                {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowRight />}
              </IconButton>
            </TableCell>
            {columns.map((column, index) => {
              const value = row[column.field]
              if (!column.hide) {
                return (
                  <TableCell
                    key={`${index}${row.occurrenceDbId}${value}`.toString()}
                    style={{ width: `${columnWidth}%`.toString() }}
                  >
                    {value}
                  </TableCell>
                )
              }
            })}
          </TableRow>
          <TableRow>
            <TableCell style={styleCell} colSpan={numberVisibleColumn + 1}>
              <Collapse in={open} timeout='auto' unmountOnExit>
                <Table size='small' aria-label='purchases'>
                  <TableBody>
                    {rowsDetails.map((rowDetail) => {
                      return (
                        <TableRow
                          key={`rowDetail${rowDetail.occurrenceDbId}`.toString()}
                        >
                          <RowDetailMolecule
                            rowDetail={rowDetail}
                            columnsDetails={columnsDetails}
                            accionSave={accionSave}
                            onlyOneExperiment={true}
                            functionGetTrait={functionGetTrait}
                            numberVisibleColumn={numberVisibleColumn}
                            columnWidth={columnWidth}
                            actionColumnWidth={actionColumnWidth}
                          />
                        </TableRow>
                      )
                    })}
                  </TableBody>
                </Table>
              </Collapse>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableCell>
  )
})
// Type and required properties
RowMasterMolecule.propTypes = {
  row: PropTypes.object.isRequired,
  columns: PropTypes.array.isRequired,
  functionDataDetails: PropTypes.func.isRequired,
  columnsDetails: PropTypes.array.isRequired,
  functionGetTrait: PropTypes.func.isRequired,
  numberVisibleColumn: PropTypes.number.isRequired,
  columnWidth: PropTypes.number.isRequired,
  actionColumnWidth: PropTypes.number.isRequired,
}
// Default properties
RowMasterMolecule.defaultProps = {}

export default RowMasterMolecule
