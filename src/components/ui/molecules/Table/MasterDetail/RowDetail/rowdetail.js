import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { TableCell, Table, TableBody, TableRow } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { grey } from '@material-ui/core/colors'
import Radio from '@material-ui/core/Radio'
import CellStatusMolecule from '../CellStatus'

function validateCheckRow(
  rowDetail,
  experiments,
  functionGetTrait,
  severity = 'warning',
) {
  let isValidateCheckRow = {
    isCheckDisable: false,
    message: '',
    severity: severity,
  }
  let isSameExperiment =
    experiments.length > 0
      ? experiments[0].experimentDbId === rowDetail.experimentDbId
        ? true
        : false
      : true
  let isIntersectionTrait = functionGetTrait(
    rowDetail.traits,
    experiments.length > 0 ? experiments.map((item) => item.traits) : [],
  )
  let isEmptyTrait = rowDetail.traits.length === 0

  if (!isSameExperiment) {
    isValidateCheckRow.message = 'This occurrence is not the same experiment'
    isValidateCheckRow.isCheckDisable = true
  }
  if (!isValidateCheckRow.isCheckDisable && !isIntersectionTrait.isIntersection) {
    isValidateCheckRow.message = 'This occurrence has not Crop trait in common'
    isValidateCheckRow.isCheckDisable = true
  }
  if (!isValidateCheckRow.isCheckDisable && isEmptyTrait) {
    isValidateCheckRow.message = 'This occurrence has not Crop trait'
    isValidateCheckRow.isCheckDisable = true
  }
  return isValidateCheckRow
}
const styleCell = {
  paddingRight: 0,
  paddingLeft: 0,
  paddingBottom: 0,
  paddingTop: 0,
  width: '100%',
}
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const RowDetailMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    rowDetail,
    columnsDetails,
    accionSave,
    onlyOneExperiment,
    functionGetTrait,
    numberVisibleColumn,
    columnWidth,
    actionColumnWidth,
    ...rest
  } = props
  const [check, setCheck] = React.useState(false)
  const dispatch = useDispatch()
  const experiments = useSelector((state) => state.newRequest.experiment)
  const isValidateCheckRow = validateCheckRow(
    rowDetail,
    experiments,
    functionGetTrait,
  )

  const handelCheck = () => {
    dispatch({
      type: accionSave,
      payload: {
        add: !check,
        data: rowDetail,
      },
    })
    setCheck(!check)
  }
  return (
    /* 
     @prop data-testid: Id to use inside rowdetail.test.js file.
     */
    <TableCell style={styleCell} ref={ref}>
      <Table size='small' aria-label='purchases'>
        <TableBody>
          <TableRow
            hover
            role='checkbox'
            tabIndex={-1}
            style={{ backgroundColor: grey[300] }}
          >
            <TableCell
              style={{
                width: `${actionColumnWidth / 2}%`.toString(),
                paddingRight: 0,
                paddingLeft: 0,
                paddingBottom: 0,
                paddingTop: 0,
              }}
            >
              <Radio
                checked={check}
                onClick={handelCheck}
                disabled={isValidateCheckRow.isCheckDisable}
              />
            </TableCell>
            <TableCell
              style={{
                width: `${actionColumnWidth / 2}%`.toString(),
                paddingRight: 0,
                paddingLeft: 0,
                paddingBottom: 0,
                paddingTop: 0,
              }}
            >
              <CellStatusMolecule
                isError={isValidateCheckRow.isCheckDisable}
                severity={isValidateCheckRow.severity}
                message={isValidateCheckRow.message}
              />
            </TableCell>
            {columnsDetails.map((columnDetail, index) => {
              let key = `${rowDetail[columnDetail.field]}_${
                rowDetail.occurrenceDbId
              }_${index}`.toString()
              if (!columnDetail.hide) {
                return (
                  <TableCell
                    key={key}
                    style={{
                      width: `${columnWidth}%`.toString(),
                      wordWrap: 'break-word',
                    }}
                  >
                    {rowDetail[columnDetail.field]}
                  </TableCell>
                )
              }
            })}
          </TableRow>
        </TableBody>
      </Table>
    </TableCell>
  )
})
// Type and required properties
RowDetailMolecule.propTypes = {
  rowDetail: PropTypes.object.isRequired,
  columnsDetails: PropTypes.array.isRequired,
  accionSave: PropTypes.string.isRequired,
  onlyOneExperiment: PropTypes.bool.isRequired,
  functionGetTrait: PropTypes.func.isRequired,
  numberVisibleColumn: PropTypes.number.isRequired,
  columnWidth: PropTypes.number.isRequired,
  actionColumnWidth: PropTypes.number.isRequired,
}
// Default properties
RowDetailMolecule.defaultProps = {
  onlyOneExperiment: false,
}

export default RowDetailMolecule
