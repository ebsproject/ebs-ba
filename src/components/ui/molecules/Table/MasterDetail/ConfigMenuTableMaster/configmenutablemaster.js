import React, { useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import {
  AppBar,
  Checkbox,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Switch,
  Toolbar,
  Typography,
} from '@material-ui/core'
import SettingsIcon from '@material-ui/icons/Settings'
import CloseIcon from '@material-ui/icons/Close'
import ItemCheckAtom from 'components/ui/atoms/ItemCheck'
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ConfigMenuTableMasterMolecule = React.forwardRef((props, ref) => {
  const { children, columns, classes, ...rest } = props

  const [open, setOpen] = useState(false)

  const handleClose = () => {
    setOpen(false)
  }

  const handleConfigButtonClick = () => {
    setOpen(true)
  }

  return (
    /* 
     @prop data-testid: Id to use inside configmenutablemaster.test.js file.
     */
    <Grid container ref={ref} data-testid={'ConfigMenuTableMasterTestId'}>
      <Grid item xs={12}>
        <IconButton onClick={handleConfigButtonClick}>
          <SettingsIcon color={'primary'} />
        </IconButton>
        <Dialog
          onClose={handleClose}
          aria-labelledby='dialog-configuration-column'
          open={open}
          fullWidth={true}
          maxWidth={'sm'}
        >
          <DialogTitle id='dialog-configuration-column-title'>
            <AppBar position='relative' className={classes.configMenuApp}>
              <Toolbar>
                <Typography variant='body1'>Costumize columns</Typography>
                <IconButton
                  className={classes.configMenuAppIcon}
                  edge='end'
                  color='inherit'
                  onClick={handleClose}
                  aria-label='close'
                >
                  <CloseIcon />
                </IconButton>
              </Toolbar>
            </AppBar>
          </DialogTitle>
          <DialogContent>
            <List>
              {columns.map((column) => (
                <ListItem key={column.field}>
                  <ItemCheckAtom
                    label={column.headerName}
                    ischeck={!column.hide}
                    classes={classes}
                  />
                </ListItem>
              ))}
            </List>
          </DialogContent>
        </Dialog>
      </Grid>
    </Grid>
  )
})
// Type and required properties
ConfigMenuTableMasterMolecule.propTypes = {
  columns: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
ConfigMenuTableMasterMolecule.defaultProps = {}

export default ConfigMenuTableMasterMolecule
