import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid, IconButton, Snackbar } from '@material-ui/core'
import ReportProblemIcon from '@material-ui/icons/ReportProblem'
import CloseIcon from '@material-ui/icons/Close'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const CellStatusMolecule = React.forwardRef((props, ref) => {
  const { children, isError, severity, message, ...rest } = props
  const [open, setOpen] = React.useState(false)
  const handelClick = () => {
    setOpen(true)
  }
  const handleClose = (event, reason) => {
    setOpen(false)
  }

  return (
    /* 
     @prop data-testid: Id to use inside cellstatus.test.js file.
     */
    <div>
      {isError ? (
        <>
          <IconButton
            size='small'
            aria-label='close'
            color='inherit'
            onClick={handelClick}
          >
            <ReportProblemIcon fontSize='small' />
          </IconButton>
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            open={open}
            autoHideDuration={6000}
            onClose={handleClose}
            message={message}
            severity={severity}
            action={
              <React.Fragment>
                <IconButton
                  size='small'
                  aria-label='close'
                  color='inherit'
                  onClick={handleClose}
                >
                  <CloseIcon fontSize='small' />
                </IconButton>
              </React.Fragment>
            }
          />
        </>
      ) : null}
    </div>
  )
})
// Type and required properties
CellStatusMolecule.propTypes = {
  isError: PropTypes.bool.isRequired,
  severity: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
}
// Default properties
CellStatusMolecule.defaultProps = {
  isError: false,
  severity: 'warning',
  message: '',
}

export default CellStatusMolecule
