import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid, TablePagination, Typography, Paper } from '@material-ui/core'
import ConfigMenuTableMasterMolecule from '../ConfigMenuTableMaster'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const HeadTableMasterMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    title,
    paginationSizes,
    totalCount,
    rowsPerPage,
    page,
    handleChangePage,
    handleChangeRowsPerPage,
    columns,
    classes,
    ...rest
  } = props

  return (
    /* 
     @prop data-testid: Id to use inside headtablemaster.test.js file.
     */
    <Grid container ref={ref} data-testid={'HeadTableMasterTestId'}>
      <Grid item xs={12}>
        <Grid container spacing={10}>
          <Grid item xs={11}>
            <Typography variant='h6'>{title}</Typography>
          </Grid>
          <Grid item xs={1}>
            <ConfigMenuTableMasterMolecule columns={columns} classes={classes} />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item>
            <TablePagination
              component={Paper}
              rowsPerPageOptions={paginationSizes}
              count={totalCount}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
})
// Type and required properties
HeadTableMasterMolecule.propTypes = {
  title: PropTypes.string.isRequired,
  paginationSizes: PropTypes.array.isRequired,
  totalCount: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  handleChangePage: PropTypes.func.isRequired,
  handleChangeRowsPerPage: PropTypes.func.isRequired,
  columns: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
HeadTableMasterMolecule.defaultProps = {}

export default HeadTableMasterMolecule
