import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableContainer,
  TablePagination,
  TableRow,
  Typography,
} from '@material-ui/core'
import RowMasterMolecule from '../RowMaster'
import MessageRowAtom from 'components/ui/atoms/MessageRow'
import HeadColumnMasterMolecule from '../HeadColumnMaster'
import HeadTableMasterMolecule from '../HeadTableMaster'

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index])
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0])
    if (order !== 0) return order
    return a[1] - b[1]
  })
  return stabilizedThis.map((el) => el[0])
}

function filter(arrays, valueFilter, columnFilter) {
  if (valueFilter !== '' && columnFilter !== '') {
    return arrays.filter((array) => array[columnFilter].includes(valueFilter))
  }
  return arrays
}
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const TableMDMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    functionData,
    columns,
    functionDataDetails,
    columnsDetails,
    title,
    accionSave,
    functionGetTrait,
    classes,
    ...rest
  } = props
  const actionColumnWidth = 8
  const paginationSizes = [10, 25, 50]
  const [rows, setRows] = useState([])
  const [isFirstTimeRows, setIsFirstTimeRows] = useState(false)
  const [messageError, setMessageError] = useState('')
  const [totalCount, setTotalCount] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [page, setPage] = useState(0)
  const [order, setOrder] = useState('asc')
  const [orderBy, setOrderBy] = useState('experimentName')
  const [valueFilter, setValueFilter] = useState('')
  const [columnFilter, setColumnFilter] = useState('')
  const numberVisibleColumn = columns.filter((column) => !column.hide).length
  const columnWidth =
    (100 - actionColumnWidth) / columns.filter((column) => !column.hide).length

  useEffect(() => {
    if (rows.length === 0 && !isFirstTimeRows)
      functionData(rowsPerPage, 1).then((res) => {
        if (res.status.status === 200) {
          setRows(res.data)
          setIsFirstTimeRows(true)
          setPage(0)
          setTotalCount(res.metadata.pagination.totalCount)
        } else {
          setMessageError(res.status.message)
        }
      })
  }, [
    rows,
    isFirstTimeRows,
    functionData,
    setRows,
    setIsFirstTimeRows,
    setPage,
    setTotalCount,
    rowsPerPage,
  ])

  const handleChangePage = (event, newPage) => {
    let pageService = newPage
    pageService++
    functionData(rowsPerPage, pageService).then((res) => {
      if (res.status.status === 200) {
        setRows(res.data)
        setPage(newPage)
        setTotalCount(res.metadata.pagination.totalCount)
      } else {
        setMessageError(res.status.message)
      }
    })
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    functionData(parseInt(event.target.value, 10), 1).then((res) => {
      if (res.status.status === 200) {
        setRows(res.data)
        setPage(0)
        setTotalCount(res.metadata.pagination.totalCount)
      } else {
        setMessageError(res.status.message)
      }
    })
  }

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const handleSetFilter = (event, property) => {
    setValueFilter(event.target.value)
    setColumnFilter(property)
  }

  return (
    /*  
     @prop data-testid: Id to use inside tablemd.test.js file.
    */
    <Grid container ref={ref} data-testid={'TableMDTestId'}>
      <Grid item xs={12}>
        <HeadTableMasterMolecule
          title={title}
          paginationSizes={paginationSizes}
          totalCount={totalCount}
          rowsPerPage={rowsPerPage}
          page={page}
          handleChangePage={handleChangePage}
          handleChangeRowsPerPage={handleChangeRowsPerPage}
          columns={columns}
          classes={classes}
        />
        <TableContainer style={{ width: '100%' }} component={Paper}>
          <Table style={{ width: '100%' }}>
            <HeadColumnMasterMolecule
              columns={columns}
              onRequestSort={handleRequestSort}
              onFilterColumn={handleSetFilter}
              order={order}
              orderBy={orderBy}
              rowCount={columns.length}
              columnWidth={columnWidth}
              actionColumnWidth={actionColumnWidth}
              classes={classes}
            />
            <TableBody>
              {filter(
                stableSort(rows, getComparator(order, orderBy)),
                valueFilter,
                columnFilter,
              ).map((row) => {
                return (
                  <TableRow key={`mianRow${row.experimentDbId}`.toString()}>
                    <RowMasterMolecule
                      row={row}
                      columns={columns}
                      functionDataDetails={functionDataDetails}
                      columnsDetails={columnsDetails}
                      accionSave={accionSave}
                      functionGetTrait={functionGetTrait}
                      numberVisibleColumn={numberVisibleColumn}
                      columnWidth={columnWidth}
                      actionColumnWidth={actionColumnWidth}
                    />
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
          <MessageRowAtom
            message={messageError}
            numberVisibleColumn={numberVisibleColumn}
          ></MessageRowAtom>
        </TableContainer>
      </Grid>
    </Grid>
  )
})
// Type and required properties
TableMDMolecule.propTypes = {
  functionData: PropTypes.func.isRequired,
  columns: PropTypes.array.isRequired,
  functionDataDetails: PropTypes.func,
  columnsDetails: PropTypes.array,
  title: PropTypes.string.isRequired,
  accionSave: PropTypes.string.isRequired,
  functionGetTrait: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
TableMDMolecule.defaultProps = {}

export default TableMDMolecule
