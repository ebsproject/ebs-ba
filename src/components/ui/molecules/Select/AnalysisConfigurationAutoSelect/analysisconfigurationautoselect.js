import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid } from '@material-ui/core'
import { useDispatch } from 'react-redux'
import SimpleSelectAtom from 'components/ui/atoms/Select/SimpleSelect'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AnalysisConfigurationAutoSelectMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    id,
    title,
    actionSave,
    valueDisplay,
    functionData,
    occurrences,
    experimentLocationAnalysisPattern,
    traitAnalysisPattern,
    analysisObjective,
    classes,
    ...rest
  } = props

  const dispatch = useDispatch()
  const idSelect = `${id}-analysis-simple-select`.toString()
  const [messageError, setMessageError] = useState('')
  const [valueDisplays, setValueDisplays] = useState([])
  const [isRequest, setIsRequest] = useState(false)
  const [isOnlyOneValue, setIsOnlyOneValue] = useState(false)
  const [exp, setExp] = useState('')
  const [trait, setTrait] = useState('')

  useEffect(() => {
    if (experimentLocationAnalysisPattern !== '' && traitAnalysisPattern !== '') {
      if (!isRequest) {
        functionData(traitAnalysisPattern, experimentLocationAnalysisPattern).then(
          (res) => {
            setIsRequest(true)
            if (res.status.status === 200) {
              setValueDisplays(res.data)
              setExp(experimentLocationAnalysisPattern)
              setTrait(traitAnalysisPattern)
            } else {
              setMessageError(res.status.message)
            }
          },
        )
      } else {
        if (
          exp != experimentLocationAnalysisPattern ||
          trait != traitAnalysisPattern
        ) {
          setIsRequest(false)
          dispatch({
            type: actionSave,
            payload: 0,
          })
        }
      }
    }
  }, [
    functionData,
    setValueDisplays,
    setMessageError,
    occurrences,
    setIsRequest,
    isRequest,
    experimentLocationAnalysisPattern,
    traitAnalysisPattern,
    analysisObjective,
    exp,
    setExp,
    trait,
    setTrait,
    dispatch,
    actionSave,
  ])

  useEffect(() => {
    if (valueDisplays.length === 1 && isRequest && !isOnlyOneValue) {
      setIsOnlyOneValue(true)
      dispatch({
        type: actionSave,
        payload: valueDisplays[0].propertyId,
      })
    }
  }, [
    valueDisplays,
    isRequest,
    isOnlyOneValue,
    setIsOnlyOneValue,
    dispatch,
    actionSave,
  ])

  const handleChange = (event) => {
    dispatch({
      type: actionSave,
      payload: event.target.value,
    })
  }

  return (
    /* 
     @prop data-testid: Id to use inside analysisconfigurationautoselect.test.js file.
     */
    <div ref={ref} data-testid={'AnalysisConfigurationAutoSelectTestId'}>
      <SimpleSelectAtom
        idselect={idSelect}
        valuedisplay={valueDisplay}
        handlechange={handleChange}
        messageerror={messageError}
        valuedisplays={valueDisplays}
        valueid={'propertyId'}
        valuelabel={'propertyName'}
        title={title}
        classes={classes}
      />
    </div>
  )
})
// Type and required properties
AnalysisConfigurationAutoSelectMolecule.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  actionSave: PropTypes.string.isRequired,
  functionData: PropTypes.func.isRequired,
  occurrences: PropTypes.array.isRequired,
  experimentLocationAnalysisPattern: PropTypes.string.isRequired,
  traitAnalysisPattern: PropTypes.string.isRequired,
  analysisObjective: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
AnalysisConfigurationAutoSelectMolecule.defaultProps = {}

export default AnalysisConfigurationAutoSelectMolecule
