import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid } from '@material-ui/core'
import { useDispatch } from 'react-redux'
import SimpleSelectAtom from 'components/ui/atoms/Select/SimpleSelect'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AnalysisConfigurationDetailAutoSelectMolecule = React.forwardRef(
  (props, ref) => {
    const {
      children,
      id,
      title,
      actionSave,
      valueDisplay,
      functionData,
      occurrences,
      analysisConfiguration,
      classes,
      ...rest
    } = props

    const dispatch = useDispatch()
    const idSelect = `${id}-analys-config-simple-select`.toString()
    const [valueDisplays, setValueDisplays] = useState([])
    const [messageError, setMessageError] = useState('')
    const [isRequest, setIsRequest] = useState(false)
    const [isOnlyOneValue, setIsOnlyOneValue] = useState(false)
    const [analysisConfigurationLocal, setAnalysisConfigurationLocal] = useState(0)

    useEffect(() => {
      if (analysisConfiguration !== 0) {
        if (!isRequest) {
          functionData(occurrences, analysisConfiguration).then((res) => {
            setIsRequest(true)
            if (res.status.status === 200) {
              setValueDisplays(res.data)
              setAnalysisConfigurationLocal(analysisConfiguration)
            } else {
              setMessageError(res.status.message)
            }
          })
        } else {
          if (analysisConfigurationLocal !== analysisConfiguration) {
            setIsRequest(false)
            setIsOnlyOneValue(false)
            dispatch({
              type: actionSave,
              payload: 0,
            })
          }
        }
      } else {
        setIsOnlyOneValue(false)
        setValueDisplays([])
        dispatch({
          type: actionSave,
          payload: 0,
        })
      }
    }, [
      functionData,
      setValueDisplays,
      setMessageError,
      occurrences,
      setIsRequest,
      isRequest,
      analysisConfiguration,
      analysisConfigurationLocal,
      setAnalysisConfigurationLocal,
      actionSave,
      dispatch,
      setIsOnlyOneValue,
    ])

    useEffect(() => {
      if (valueDisplays.length === 1 && isRequest && !isOnlyOneValue) {
        setIsOnlyOneValue(true)
        dispatch({
          type: actionSave,
          payload: valueDisplays[0].propertyId,
        })
      }
    }, [
      valueDisplays,
      isRequest,
      isOnlyOneValue,
      setIsOnlyOneValue,
      dispatch,
      actionSave,
    ])

    const handleChange = (event) => {
      dispatch({
        type: actionSave,
        payload: event.target.value,
      })
    }
    const existValueDisplay = valueDisplays.some(
      (value) => value.propertyId === valueDisplay,
    )

    return (
      /* 
       @prop data-testid: Id to use inside analysisconfigurationdetailautoselect.test.js file.
       */
      <div ref={ref} data-testid={'AnalysisConfigurationDetailAutoSelectTestId'}>
        <SimpleSelectAtom
          idselect={idSelect}
          valuedisplay={existValueDisplay ? valueDisplay : ''}
          handlechange={handleChange}
          messageerror={messageError}
          valuedisplays={valueDisplays}
          title={title}
          valueid={'propertyId'}
          valuelabel={'propertyName'}
          classes={classes}
        />
      </div>
    )
  },
)
// Type and required properties
AnalysisConfigurationDetailAutoSelectMolecule.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  actionSave: PropTypes.string.isRequired,
  functionData: PropTypes.func.isRequired,
  occurrences: PropTypes.array.isRequired,
  analysisConfiguration: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
AnalysisConfigurationDetailAutoSelectMolecule.defaultProps = {}

export default AnalysisConfigurationDetailAutoSelectMolecule
