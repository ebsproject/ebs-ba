import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid, TextField, Typography } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { makeStyles } from '@material-ui/core/styles'
import { useDispatch } from 'react-redux'

const useStyles = makeStyles((theme) => ({
  root: {
    width: 500,
    '& > * + *': {
      marginTop: theme.spacing(3),
    },
  },
}))

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const TraitAutoSelectMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    actionSave,
    responseVariableTrait,
    functionData,
    occurrences,
    ...rest
  } = props

  const classes = useStyles()
  const dispatch = useDispatch()
  const [responseVariableTraits, setResponseVariableTraits] = React.useState([])
  const [request, setRequest] = React.useState(false)

  useEffect(() => {
    if (responseVariableTraits.length === 0 && !request) {
      let traits = functionData(occurrences)
      setResponseVariableTraits(traits)
      setRequest(true)
    }
  }, [
    functionData,
    setResponseVariableTraits,
    occurrences,
    responseVariableTraits,
    request,
    setRequest,
  ])

  const traitHandleChange = (event, value, reason) => {
    //console.log('value', value, 'reason', reason)
    dispatch({
      type: actionSave,
      payload: value,
    })
  }

  return (
    /* 
     @prop data-testid: Id to use inside traitautoselect.test.js file.
     */
    <Grid
      container
      ref={ref}
      direction='row'
      justify='flex-start'
      alignItems='flex-start'
      data-testid={'TraitAutoSelectTestId'}
    >
      <div className={classes.root}>
        <Typography variant='body1'>{'Response variables'}</Typography>
        <Autocomplete
          multiple
          id='tags-outlined'
          options={responseVariableTraits}
          getOptionLabel={(responseVariableTrait) => responseVariableTrait.title}
          filterSelectedOptions
          size='small'
          onChange={traitHandleChange}
          renderInput={(params) => (
            <TextField
              {...params}
              variant='outlined'
              //label='filterSelectedOptions'
              placeholder='Traits'
            />
          )}
        />
      </div>
    </Grid>
  )
})
// Type and required properties
TraitAutoSelectMolecule.propTypes = {
  actionSave: PropTypes.string.isRequired,
  responseVariableTrait: PropTypes.array.isRequired,
  functionData: PropTypes.func.isRequired,
  occurrences: PropTypes.array.isRequired,
}
// Default properties
TraitAutoSelectMolecule.defaultProps = {
  responseVariableTrait: [],
}

export default TraitAutoSelectMolecule
