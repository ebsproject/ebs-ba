import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid } from '@material-ui/core'
import { useDispatch } from 'react-redux'
import SimpleSelectAtom from 'components/ui/atoms/Select/SimpleSelect'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AutoSelectMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    id,
    title,
    actionSave,
    valueDisplay,
    functionData,
    occurrences,
    classes,
    ...rest
  } = props

  const dispatch = useDispatch()
  const idSelect = `${id}-simple-select`.toString()
  const [valueDisplays, setValueDisplays] = useState([])
  const [messageError, setMessageError] = useState('')
  const [isRequest, setIsRequest] = useState(false)
  const [isOnlyOneValue, setIsOnlyOneValue] = useState(false)

  useEffect(() => {
    if (valueDisplays.length === 0 && !isRequest) {
      functionData(occurrences).then((res) => {
        setIsRequest(true)
        if (res.status.status === 200) {
          setValueDisplays(res.data)
        } else {
          setMessageError(res.status.message)
        }
      })
    }
  }, [
    functionData,
    setValueDisplays,
    setMessageError,
    occurrences,
    setIsRequest,
    isRequest,
    valueDisplays,
  ])

  useEffect(() => {
    if (valueDisplays.length === 1 && isRequest && !isOnlyOneValue) {
      setIsOnlyOneValue(true)
      dispatch({
        type: actionSave,
        payload: valueDisplays[0].propertyId,
      })
    }
  }, [
    valueDisplays,
    isRequest,
    isOnlyOneValue,
    setIsOnlyOneValue,
    dispatch,
    actionSave,
  ])

  const handleChange = (event) => {
    dispatch({
      type: actionSave,
      payload: event.target.value,
    })
  }

  return (
    /* 
     @prop data-testid: Id to use inside autoselect.test.js file.
     */
    <div ref={ref} data-testid={'AutoSelectTestId'}>
      <SimpleSelectAtom
        idselect={idSelect}
        valuedisplay={valueDisplay}
        handlechange={handleChange}
        messageerror={messageError}
        valuedisplays={valueDisplays}
        valueid={'propertyId'}
        valuelabel={'propertyName'}
        title={title}
        classes={classes}
      />
    </div>
  )
})
// Type and required properties
AutoSelectMolecule.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  actionSave: PropTypes.string.isRequired,
  functionData: PropTypes.func.isRequired,
  occurrences: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
AutoSelectMolecule.defaultProps = {}

export default AutoSelectMolecule
