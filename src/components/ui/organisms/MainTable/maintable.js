import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND MOLECULES TO USE
import { Grid, IconButton, Typography } from '@material-ui/core'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'
import MainDataGridMolecule from 'components/ui/molecules/MainDataGrid'
import VisibilityIcon from '@material-ui/icons/Visibility'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const MainTableOrganism = React.forwardRef((props, ref) => {
  const {
    children,
    functiondata,
    title,
    columnsdata,
    buttons,
    staterequest,
    classes,
    ...rest
  } = props
  const newRequestButton = buttons.find((button) => button.id === 'newRequest')
  const removeRequestButton = buttons.find((button) => button.id === 'removeRequest')
  const history = useHistory()
  const dispatch = useDispatch()

  const handleNewRequest = () => {
    dispatch({
      type: newRequestButton.parameters.actionSave,
      payload: '',
    })
    history.push(newRequestButton.parameters.ruteToPage)
  }
  const handleRemoveRequest = () => {
    alert('Function not implemented')
  }

  const toolbarActions = () => {
    return (
      <div>
        <IconButton
          title={newRequestButton.title}
          color={newRequestButton.color}
          onClick={handleNewRequest}
        >
          <Typography variant='button'>{newRequestButton.title}</Typography>
        </IconButton>
        <IconButton
          title={removeRequestButton.title}
          color={removeRequestButton.color}
          onClick={handleRemoveRequest}
        >
          <DeleteIcon />
        </IconButton>
      </div>
    )
  }

  const rowActions = (rowData, refresh) => {
    const handleClick = () => {
      alert(rowData.status)
    }
    return (
      <Grid container>
        <Grid item>
          <IconButton
            size='small'
            onClick={handleClick}
            color='primary'
            disabled={rowData.status === staterequest.draft ? false : true}
          >
            <EditIcon />
          </IconButton>
        </Grid>
        <Grid item>
          <IconButton
            size='small'
            onClick={handleClick}
            color='primary'
            disabled={
              rowData.status === staterequest.processing
                ? false
                : rowData.status === staterequest.complete
                ? false
                : true
            }
          >
            <VisibilityIcon />
          </IconButton>
        </Grid>
        <Grid item>
          <IconButton
            size='small'
            onClick={handleClick}
            color='primary'
            disabled={rowData.status === staterequest.complete ? false : true}
          >
            <CloudDownloadIcon />
          </IconButton>
        </Grid>
      </Grid>
    )
  }
  return (
    /* 
     @prop data-testid: Id to use inside maintable.test.js file.
     */
    <Grid container ref={ref} data-testid={'MainTableTestId'}>
      <Grid item xs={12}>
        <MainDataGridMolecule
          functiondata={functiondata}
          toolbaractions={toolbarActions}
          title={title}
          rowactions={rowActions}
          columns={columnsdata}
          classes={classes}
          staterequest={staterequest}
        />
      </Grid>
    </Grid>
  )
})
// Type and required properties
MainTableOrganism.propTypes = {
  functiondata: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  columnsdata: PropTypes.array.isRequired,
  buttons: PropTypes.array.isRequired,
  staterequest: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
MainTableOrganism.defaultProps = {}

export default MainTableOrganism
