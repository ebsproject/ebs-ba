import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND MOLECULES TO USE
import { AppBar, Box, Grid } from '@material-ui/core'
import PopUpTokenButtonMolecule from 'components/ui/molecules/button/PopUpTokenButton'

const getButton = ({ id, label, active, parameter }) => {
  switch (id) {
    case 'getTokenId':
      return active ? (
        <PopUpTokenButtonMolecule
          id={id}
          label={label}
          idstorage={parameter.idStorage}
          uriparent={parameter.uriParent}
        />
      ) : (
        <></>
      )
    default:
      return <></>
  }
}
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const MainNavBarOrganism = React.forwardRef((props, ref) => {
  const { children, buttons, classes, ...rest } = props
  buttons.forEach((element) => {
    element.button = getButton({
      id: element.id,
      label: element.label,
      active: element.active,
      parameter: element.parameter,
    })
  })
  return (
    /* 
     @prop data-testid: Id to use inside mainnavbar.test.js file.
     */
    <div ref={ref} data-testid={'MainNavBarTestId'}>
      <AppBar key='appbar_nabar_main' position='static' color='inherit'>
        <Box key='box_title_nav_bar_main' display='flex' p={1}>
          <Box key='box_button_nav_bar_main' p={1} flexGrow={1} />
          {buttons.map((button) => {
            return <Box key={button.id}> {button.button} </Box>
          })}
        </Box>
      </AppBar>
    </div>
  )
})
// Type and required properties
MainNavBarOrganism.propTypes = {
  buttons: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
MainNavBarOrganism.defaultProps = {}

export default MainNavBarOrganism
