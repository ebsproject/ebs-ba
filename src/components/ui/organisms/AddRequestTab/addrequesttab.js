import React, { useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND MOLECULES TO USE
import { Grid, Tab, Tabs } from '@material-ui/core'
import SubTitleMolecule from 'components/ui/molecules/SubTitle'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import ExperimentsTabPanelMolecule from 'components/ui/molecules/TabPanel/ExperimentsTabPanel'
import MenuButtonTabMolecule from 'components/ui/molecules/TabPanel/MenuButtonTab'
import SettingsTabPanelMolecule from 'components/ui/molecules/TabPanel/SettingsTabPanel'
import ParametersTabPanelMolecule from 'components/ui/molecules/TabPanel/ParametersTabPanel'
import { getAtributeFromJsonString } from 'utils/helpers/coreHelper'

// Get the error per item, that not added this in stepper
function getMessageFinish(selects, experiment) {
  let messages = [
    {
      step: 0,
      message:
        experiment.experiment.length === 0
          ? 'Please select one or more experiment'
          : '',
      data: {
        experiment: experiment.experiment.length,
        experimentNameFirst:
          experiment.experiment.length > 0
            ? experiment.experiment[0].experiment
            : '',
      },
    },
    {
      step: 1,
      message: getMessageStep1(selects),
      data: {},
    },
    {
      step: 2,
      message: getMessageStep2(selects),
      data: {},
    },
    {
      step: 3,
      message: '',
      data: {},
    },
  ]
  return messages
}

function getMessageStep1(selects) {
  if (selects.responseVariableTrait.responseVariableTrait.length === 0)
    return 'Please select one Response Variables'
  if (
    selects.analysisObjective.analysisObjective === 0 ||
    selects.analysisObjective.analysisObjective === ''
  )
    return 'Please select one Analysis Objective'
  return ''
}

function getMessageStep2(selects) {
  if (
    selects.traitAnalysisPattern.traitAnalysisPattern === 0 ||
    selects.traitAnalysisPattern.traitAnalysisPattern === ''
  )
    return 'Please select Analysis Pattern'
  if (
    selects.experimentLocationAnalysisPattern.experimentLocationAnalysisPattern ===
      0 ||
    selects.experimentLocationAnalysisPattern.experimentLocationAnalysisPattern ===
      ''
  )
    return 'Please select Experiment Location Analysis Pattern'
  if (
    selects.analysisConfiguration.analysisConfiguration === 0 ||
    selects.analysisConfiguration.analysisConfiguration === ''
  )
    return 'Please select Analysis Configuration'
  if (selects.mainModel.mainModel === 0 || selects.mainModel.mainModel === '')
    return 'Please select Main Model'
  //# To habilite prediction
  //if (selects.prediction.prediction === 0) return 'Please select Prediction'
  if (
    selects.spatialAdjusting.spatialAdjusting === 0 ||
    selects.spatialAdjusting.spatialAdjusting === ''
  )
    return 'Please select Spatial Adjusting'
  return ''
}

function getMenuButtonConfig(messages, value, tapsLength) {
  let message = messages.find((x) => x.step === value)
  let configButton = {
    backDisable: value > 1 ? false : true,
    nextDisable: message.message === '' ? false : true,
    nextText: value + 1 === tapsLength ? 'Finish' : 'Next',
  }
  return configButton
}

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AddRequestTabOrganism = React.forwardRef((props, ref) => {
  const {
    children,
    setting,
    selects,
    experiment,
    subtitle,
    classes,
    request,
    ...rest
  } = props
  const [value, setValue] = useState(0)
  const dispatch = useDispatch()
  const history = useHistory()
  const tabsArray = ['Experiments', 'Settings', 'Parameters']

  experiment.experiment = useSelector((state) => state.newRequest.experiment)
  selects.responseVariableTrait.responseVariableTrait = useSelector(
    (state) => state.newRequest.responseVariableTrait,
  )
  selects.analysisObjective.analysisObjective = useSelector(
    (state) => state.newRequest.analysisObjective,
  )
  selects.traitAnalysisPattern.traitAnalysisPattern = useSelector(
    (state) => state.newRequest.traitAnalysisPattern,
  )
  selects.experimentLocationAnalysisPattern.experimentLocationAnalysisPattern = useSelector(
    (state) => state.newRequest.experimentLocationAnalysisPattern,
  )
  selects.analysisConfiguration.analysisConfiguration = useSelector(
    (state) => state.newRequest.analysisConfiguration,
  )
  selects.mainModel.mainModel = useSelector((state) => state.newRequest.mainModel)
  //selects.prediction.prediction = useSelector((state) => state.newRequest.prediction)
  selects.spatialAdjusting.spatialAdjusting = useSelector(
    (state) => state.newRequest.spatialAdjusting,
  )
  const messages = getMessageFinish(selects, experiment)
  let severity = 'warning'
  let message = messages.find((x) => x.step === value)
  const configButton = getMenuButtonConfig(messages, value, tabsArray.length)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const newRequestData = request.functionCreateNewRequest({
    dataSourceUrl: request.dataSourceUrl,
    token: localStorage.getItem('id_token'),
    experiments: experiment.experiment,
    responseVariableTraits: selects.responseVariableTrait.responseVariableTrait,
    analysisObjective: selects.analysisObjective.analysisObjective,
    traitAnalysisPattern: getAtributeFromJsonString(
      selects.traitAnalysisPattern.traitAnalysisPattern,
      'id',
    ),
    experimentLocationAnalysisPattern: getAtributeFromJsonString(
      selects.experimentLocationAnalysisPattern.experimentLocationAnalysisPattern,
      'id',
    ),
    analysisConfiguration: selects.analysisConfiguration.analysisConfiguration,
    spatialAdjusting: selects.spatialAdjusting.spatialAdjusting,
    prediction: request.functionGetPrediction(
      selects.experimentLocationAnalysisPattern.experimentLocationAnalysisPattern,
    ),
  })

  return (
    /* 
     @prop data-testid: Id to use inside addrequesttab.test.js file.
     */
    <Grid
      container
      ref={ref}
      data-testid={'AddRequestTabTestId'}
      className={classes.grid}
    >
      <Grid item xs={9} className={classes.grid}>
        <SubTitleMolecule
          titleText={subtitle}
          message={message.message}
          severity={severity}
        />
      </Grid>
      <Grid item xs={3} className={classes.grid}>
        <MenuButtonTabMolecule
          cancelAction={setting.action.NEW_REQUEST_ACTION}
          cancelRoute={setting.routes.rotuelist}
          functionSetTab={setValue}
          nextDisable={configButton.nextDisable}
          nextText={configButton.nextText}
          tapsLength={tabsArray.length}
          valuetab={value}
          newrequestdata={newRequestData}
          routeaftersave={setting.routes.rotuelist}
          functionsave={request.functionSaveRequest}
        />
      </Grid>
      <Grid item xs={12} className={classes.grid}>
        <Tabs
          value={value}
          indicatorColor='primary'
          textColor='primary'
          onChange={handleChange}
          aria-label='Add New Request'
        >
          {tabsArray.map((tab, index) => {
            const label = `${index + 1} ${tab}`.toString()
            return <Tab value={index} label={label} disabled key={label} />
          })}
        </Tabs>
        <ExperimentsTabPanelMolecule
          value={value}
          index={0}
          experiment={experiment}
          classes={classes}
        />
        <SettingsTabPanelMolecule
          value={value}
          index={1}
          selects={selects}
          experiment={experiment}
          classes={classes}
        />
        <ParametersTabPanelMolecule
          value={value}
          index={2}
          selects={selects}
          experiment={experiment}
          classes={classes}
        />
      </Grid>
    </Grid>
  )
})
// Type and required properties
AddRequestTabOrganism.propTypes = {
  setting: PropTypes.object.isRequired,
  selects: PropTypes.object.isRequired,
  experiment: PropTypes.object.isRequired,
  subtitle: PropTypes.string.isRequired,
  request: PropTypes.object.isRequired,
}
// Default properties
AddRequestTabOrganism.defaultProps = {
  subtitle: '',
}

export default AddRequestTabOrganism
