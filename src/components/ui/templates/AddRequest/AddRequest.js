import React from 'react'
import { Grid, makeStyles, Paper } from '@material-ui/core'
import PropTypes from 'prop-types'
import AddRequestTabOrganism from 'components/ui/organisms/AddRequestTab'

const useStyles = makeStyles((theme) => ({
  grid: {
    margin: '0 -15px',
    width: 'calc(100% + 30px)',
    padding: '0 15px !important',
  },
  table: {
    width: '100%',
  },
  tablecontainer: {
    width: '100%',
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  formSelect: {
    width: 500,
    '& > * + *': {
      marginTop: theme.spacing(3),
    },
  },
  select: {
    minWidth: 380,
  },
  configMenuApp: {
    flexFlow: 1,
  },
  configMenuAppIcon: {
    marginLeft: 'auto',
  },
  listItem: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
}))

export default function AddRequest({
  setting,
  selects,
  experiment,
  titleData,
  request,
}) {
  const classes = useStyles()

  return (
    <Grid container className={classes.grid}>
      <Grid item sm className={classes.grid}>
        <Paper>
          <AddRequestTabOrganism
            setting={setting}
            selects={selects}
            experiment={experiment}
            subtitle={titleData.subtitle}
            classes={classes}
            request={request}
          ></AddRequestTabOrganism>
        </Paper>
      </Grid>
    </Grid>
  )
}
AddRequest.prototype = {
  setting: PropTypes.object.isRequired,
  selects: PropTypes.object.isRequired,
  experiment: PropTypes.array.isRequired,
  request: PropTypes.object.isRequired,
}
