import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND MOLECULES TO USE
import { Grid, Paper } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { green, indigo, lightBlue } from '@material-ui/core/colors'
import MainTableOrganism from 'components/ui/organisms/MainTable'
import MainNavBarOrganism from 'components/ui/organisms/MainNavBar'

const useStyles = makeStyles((theme) => ({
  complete: {
    background: indigo[800],
    borderRadius: 3,
    border: 0,
    color: 'white',
    alignContent: 'center',
  },
  proccess: {
    background: lightBlue[800],
    borderRadius: 3,
    border: 0,
    color: 'white',
  },
  draft: {
    background: green[800],
    borderRadius: 3,
    border: 0,
    color: 'white',
  },
  grid: {
    margin: '0 -15px',
    width: 'calc(100% + 30px)',
    padding: '0 15px !important',
  },
}))
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const MainRequestTemplate = React.forwardRef((props, ref) => {
  const { children, requestlist, navbar, ...rest } = props
  const classes = useStyles()
  return (
    /* 
     @prop data-testid: Id to use inside mainrequest.test.js file.
     */
    <Grid
      container
      ref={ref}
      data-testid={'MainRequestTestId'}
      className={classes.grid}
    >
      <Grid item xs={12} className={classes.grid}>
        <Paper>
          <MainNavBarOrganism buttons={navbar.buttons} classes={classes} />
          <MainTableOrganism
            functiondata={requestlist.functionData}
            title={requestlist.title}
            columnsdata={requestlist.columnsData}
            buttons={requestlist.buttons}
            staterequest={requestlist.stateRequest}
            classes={classes}
          />
        </Paper>
      </Grid>
    </Grid>
  )
})
// Type and required properties
MainRequestTemplate.propTypes = {
  navbar: PropTypes.object.isRequired,
  requestlist: PropTypes.object.isRequired,
}
// Default properties
MainRequestTemplate.defaultProps = {}

export default MainRequestTemplate
